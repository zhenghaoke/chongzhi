<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect(\route('phone_bill'));
});

# 产品
Route::group([
    'prefix' => 'product',
], function (Router $router) {
    // 话费
    $router->get('phone_bill', 'Product\PhoneBillController@index')->name('phone_bill');
    $router->get('phone_bill/list', 'Product\PhoneBillController@list');

    // 流量
    $router->get('flow', 'Product\FlowController@index')->name('flow');


});

# 个人中心
Route::group([
    'middleware' => 'login',
    'prefix' => 'member',
], function (Router $router) {
    $router->get('info', 'MemberController@info');
    $router->get('/', 'MemberController@index')->name('user');
    $router->get('/level', 'MemberController@memberLevel')->name('user.level');
    $router->get('/notice', 'MemberController@notice')->name('notice');
});

# 我的钱包
Route::group([
    'prefix' => 'coin',
    'middleware' => ['jwt.user'],
], function (Router $router) {
    $router->get('/', 'CoinLogsController@index');
    $router->get('/juntai', 'CoinLogsController@juntai');
    $router->get('/tongchuang', 'CoinLogsController@tongchuang');
    $router->get('/juntai_logs', 'CoinLogsController@juntaiLogs');
    $router->get('/tongchuang_logs', 'CoinLogsController@tongchuangLogs');
    $router->get('/balance_logs', 'CoinLogsController@balanceLogs');
});

// 订单
Route::group([
    'middleware' => ['login'],
    'prefix' => 'orders',
    'as' => 'order.',
], function (Router $router) {
    $router->get('/detail', 'OrderController@detail')->name('detail');
    $router->post('/create', 'OrderController@create')->name('create');



    // 确认普通订单页
    $router->get('confirm_order', 'OrderController@confirmOrder');
    // 创建普通订单
//    $router->post('create', 'OrderController@create');
    // 订单确认收货
    $router->post('confirm_receipt', 'OrderController@confirmReceipt');
    // 删除订单
    $router->post('delete', 'OrderController@delete');
    // 订单提醒发货
    $router->post('remind', 'OrderController@remind');
    // 订单查询
    $router->get('search', 'OrderController@searchOrders');
    // 订单售后
    $router->get('after_sale', 'OrderController@afterSale');
    // 订单详情页
//    $router->get('detail', 'OrderController@orderDetail');
});

Route::group([
    'prefix' => 'pay',
], function (Router $router) {
    // 微信支付
    $router->any('wechat', 'PayController@payByWechat');
    // 微信支付回调
    $router->any('wechat/notify', 'PayController@notifyByWechat')->name('wechat.notify');


    // 第三方微信支付
    $router->post('thirdPart', 'PayController@payByThirdPart');
});

# 登录
Route::group([
    'prefix' => 'auth',
//    'middleware' => ['jwt.user'],
], function (Router $router) {
    $router->get('/wechat', 'WechatController@auth');
    $router->get('/wechat/public', 'WechatController@public');
    $router->any('/wechat/get_oauth', 'WechatController@getOAuth')->name('auth.get_oauth');
    $router->post('/mobile', 'LoginController@auth');
});

Route::post('/getCode', 'SmsController@getCode');


Route::get('/error', 'HomeController@error')->name('error');
