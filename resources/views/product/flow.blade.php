@extends('layout')

@section('title')
    <title>流量充值</title>
@endsection()

@section('content')
    <input name="title" placeholder="请输入手机号码" class="layui-input"
           lay-verType="tips" lay-verify="required" required/>
@endsection()

@section('script')
    <script>
        layui.use(['layer', 'form'], function () {
            var $ = layui.jquery;
            var layer = layui.layer;
            var form = layui.form;


            form.on('submit(formBasSubmit)', function (data) {
                layer.msg(JSON.stringify(data.field));
                return false;
            });

        });
    </script>
@endsection()
