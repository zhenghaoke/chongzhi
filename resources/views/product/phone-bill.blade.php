@extends('layout')

@section('title')
    <title>话费充值</title>
@endsection()
<style>
 [v-cloak] {
      display: none;
 }
</style>
@section('content')

    <div id="myapp" v-cloak>
        <input size=5 id="moblie" v-on:input="getArea" v-model="moblie" placeholder="请输入手机号码" class="layui-input js-mobile"
   lay-verType="tips" lay-verify="required" required/><br>
    <span class="introduce">@{{Area}}</span>
        <table class="layui-table" lay-skin="line">
            <tbody data-create="{{ route('order.create') }}">
                <template v-for="(item,index) in products_3">
                    <tr @click="checkMoblie(item.id,3)">
        				<td>
        					<span class="layui-text">@{{item.name}} <span style="color:red">@{{item.tag}} </span></span>
        					<br>
        					<span class="introduce">@{{item.detail}}</span>
        				</td>
        				<td class="price" style="width: 30%;">
        					<span class="bor">
        						<span class="price_number">@{{item.price}}</span>元
        					</span>
        				</td>
    	            </tr>
                </template>

            </tbody>
        </table>

        <div v-for="(item,index) in products_2" v-if="products_2">
            <div class="product_main weight">@{{item.name}}
                <span style="color:red">@{{item.tag}}</span>
                <a class="right weight details">详情</a>
                <p class="introduce">
                    <span size="1">@{{item.name}}</span>
                </p>
            </div>
            <div class="product" v-for="(sku_item,index) in item.product_sku">
                <div @click="checkMoblie(sku_item.sku_id,2)" class="layui-col-xs4 layui-col-sm5 layui-col-md4">
        		  <div class="product_sub">
        			  <span class="layui-badge layui-badge-blue right flag">@{{sku_item.tag}}</span>
        			  <span class="details weight">@{{sku_item.content}}</span><br>
        			  <span class="price_number">@{{sku_item.price}}</span>元
        		  </div>
        		</div>
            </div>
        </div>
     </div>
    <br><br><br><br><br>
@endsection()
<script src="https://cdn.staticfile.org/vue/3.0.11/vue.global.js"></script>
@section('script')
    <script>
    var t;
        layui.use(['jquery','layer'], function () {
            var $ = layui.jquery;
            var layer = layui.layer;
            const Counter = {
                data() {
                    return {
                        Area:'',
                        products: [],
                        moblie:'',
                    }
                },
                mounted() {
                    this.initAjax();
                    this.test();
                },
                computed: {
                    // 过滤出type=1
                    products_1: function () {
                        return this.products.filter(function (item) {
                            return item.product_type == 1
                        });
                    },
                     // 过滤出type=2
                    products_2: function () {
                        return this.products.filter(function (item) {
                            return item.product_type == 2
                        });
                    },
                    // 过滤出type=3
                    products_3: function () {
                        return this.products.filter(function (item) {
                            return item.product_type == 3
                        });
                    }
                },
                methods: {
                    initAjax: function () {
                        let that = this;
                        $.get("phone_bill/list", function (res) {
                            that.products = res.products
                        });
                    },
                    checkMoblie: function (skuId,type){
                        let that = this;
                        if (!(/^1[34578]\d{9}$/.test(that.moblie))){
                            document.getElementById('moblie').focus();
                            layer.msg("请输入正确的手机号码！");
                        }else{
                            $.post("/orders/create", {id: skuId,type:type,mobile:that.moblie}, function (res) {
                                if (res.code == 200) {
                                    //window.location.href = url +'?ordersn=' + res.order_sn;
                                    window.location.href="order?ordersn="+res.order_sn;
                                }else{
                                    alert(res.massage)
                                }
                            });
                        };
                    },
                    getArea: function () {
                        let that = this;
                        if ((/^1[34578]\d{9}$/.test(that.moblie))) {
                            $.get("http://api.qqjsz.cn/mobile/get.php?phone="+this.moblie, function (res) {
                                if(res.resultcode == 200){
                                    that.Area = res.result.province + " " +  res.result.city + " " +  res.result.company;
                                }
                            },"json");
                            $.get("phone_bill/list?mobile="+this.moblie, function (res) {
                                that.products = res.products;
                            },"json");
                        };
                    },
                    test: function(){
                        return ;
                        let that = this;
                        $.get("http://api.qqjsz.cn/mobile/get.php?phone=13736914651", function (res) {
                            if(res.resultcode == 200){
                                that.Area = res.result.province + " " +  res.result.city + " " +  res.result.company;
                            }
                        },'json');
                    }

                }
            }
            Vue.createApp(Counter).mount("#myapp")
        })
    </script>
@endsection()
