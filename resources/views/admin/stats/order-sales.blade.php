<script type="text/javascript" src="/vendor/laravel-admin/moment/min/moment-with-locales.min.js" charset="UTF-8"></script>
{{--<script type="text/javascript" src="/bootstrap-datetimepicker.js" charset="UTF-8"></script>--}}
<script type="text/javascript" src="/vendor/laravel-admin/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css:5" charset="UTF-8"></script>
<script type="text/javascript" src="/vendor/laravel-admin/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
@include('tool.table-css')
<form id="refererlog-search-form" class="js-form form-inline well well-sm" action="" method="get" novalidate="">
    <div class="form-group " style="margin-top: 10px">
        <label for="nickname" style="margin-right: 5px">昵称</label>
        <div class="form-group">
            <input class="form-control" type="text" placeholder="昵称" name="nickname" value="{{ $conditions['nickname'] ?? '' }}">
        </div>
        <label for="shopName" style="margin-left: 20px; margin-right: 5px">店铺名称/联系人</label>
        <div class="form-group">
            <input class="form-control" type="text" placeholder="店铺名称/联系人" name="shopName" value="{{ $conditions['shopName'] ?? '' }}">
        </div>
        <label for="mobile" style="margin-left: 20px; margin-right: 5px">联系方式</label>
        <div class="form-group">
            <input class="form-control" type="text" placeholder="联系方式" name="mobile" value="{{ $conditions['mobile'] ?? '' }}">
        </div>
    </div>
    <div class="form-group" style="margin-top: 10px">
        <select class="form-control" name="filterType">
            <option value="orderNum" @if(request('filterType') == 'orderNum')selected @endif>订单量</option>
            <option value="spread" @if(request('filterType') == 'spread')selected @endif>推广人数</option>
        </select>
        @include('tool.time-select', ['type' => [2,3], 'dateRange' => $dateRange])
    </div>
    <div class="form-group" style="margin-top: 10px">
        <button class="btn btn-primary" type="submit" style="margin-left: 20px">搜索</button>
    </div>
</form>

<table class="table table-striped table-hover" id="course-table" style="word-break:break-all;">
    <thead>
    <tr>
        <th>ID</th>
        <th>昵称</th>
        <th>商铺名称</th>
        <th>联系人</th>
        <th>联系方式</th>
        <th>订单量</th>
        <th>营业额</th>
        <th>推广人数</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>

    @if (!$shops)
        <tr>
            <td colspan="20">
                <div class="empty">暂无商户记录!</div>
            </td>
        </tr>
    @endif

    @foreach($shops as $shop)
        <tr>
            <td>{{ $shop['id'] }}</td>
            <td>{{ $shop['nickname'] }}</td>
            <td>{{ $shop['title'] }}</td>
            <td>{{ $shop['name'] }}</td>
            <td>{{ $shop['mobile'] }}</td>
            <td>{{ $orders[$shop['id']]['sales'] ?? 0 }}</td>
            <td>{{ $orders[$shop['id']]['price'] ?? 0 }}</td>
            <td>{{ $spread[$shop['id']]['spread'] ?? 0 }}</td>
            <td><a class="btn btn-default btn-sm" href="{{ route('admin.stats.sales.detail', ['id' => $shop['id']]) }}" target="_blank">详情</a></td>
        </tr>
    @endforeach
    </tbody>
</table>

@if ($orders)
    <div class="box-footer clearfix">
        @include('tool.paginator', ['paginator' => $paginator])
    </div>
@endif
