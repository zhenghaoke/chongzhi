<script type="text/javascript" src="/vendor/laravel-admin/moment/min/moment-with-locales.min.js" charset="UTF-8"></script>
{{--<script type="text/javascript" src="/bootstrap-datetimepicker.js" charset="UTF-8"></script>--}}
<script type="text/javascript" src="/vendor/laravel-admin/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css:5" charset="UTF-8"></script>
<script type="text/javascript" src="/vendor/laravel-admin/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
@include('tool.table-css')

<form id="refererlog-search-form" class="form-inline well well-sm" action="" method="get" novalidate="">
    <div class="form-group ">
        <label for="startDate">核销时间</label>
        <input class="form-control datetimepicker-input" type="text" id="startTime" name="startTime" value="{{ $dateRange['startTime'] }}" placeholder="起始日期">
        <label for="endDate">-</label>
        <input class="form-control datetimepicker-input" type="text" id="endTime" name="endTime" value="{{ $dateRange['endTime'] }}" placeholder="截至日期">

{{--        <button--}}
{{--            class="btn btn-default btn-data-range  @if (request('date-range') == 'day') active @endif"--}}
{{--            type="button" id="btn-yesterday-range" data-type="day" data-start="{{ $dateRange['yesterdayStart'] }}"--}}
{{--            data-end="{{ $dateRange['yesterdayEnd'] }}">昨天</button>--}}
{{--        <button--}}
{{--            class="btn btn-default btn-data-range  @if (request('date-range') == 'week') active @endif"--}}
{{--            type="button" id="btn-lastWeek-range" data-type="week" data-start="{{ $dateRange['lastWeekStart'] }}"--}}
{{--            data-end="{{ $dateRange['lastWeekEnd'] }}">最近七天</button>--}}
{{--        <button--}}
{{--            class="btn btn-default btn-data-range @if (request('date-range') == 'month') active @endif"--}}
{{--            type="button" id="btn-lastMonth-range" data-type="month" data-start="{{ $dateRange['lastMonthStart'] }}"--}}
{{--            data-end="{{ $dateRange['lastMonthEnd'] }}">最近30天</button>--}}
        <input name="date-range" type="hidden" value="{{ request('date-range', 'week') }}">

        <button class="btn btn-primary" type="submit">搜索</button>
    </div>
</form>
<script>
    var $startTime = $('#startTime');
    var $endTime = $("#endTime");
    $startTime.datetimepicker({
        format: "YYYY-MM-DD",
        locale: "zh-CN"
    }).on('changeDate', function () {
        $endTime.datetimepicker('setStartDate', $startTime.val());
        $endTime.datetimepicker('setEndDate', nextYear($startTime.val()));
    });

    $startTime.datetimepicker('setEndDate', $endTime.val());

    $endTime.datetimepicker({
        format: "YYYY-MM-DD",
        locale: "zh-CN"
    }).on('changeDate', function () {
        $startTime.datetimepicker('setEndDate', $endTime.val());
        $startTime.datetimepicker('setStartDate', prevYear($endTime.val()));
    });

    $endTime.datetimepicker('setStartDate', $startTime.val());
</script>

<table class="table table-striped table-hover" id="course-table" style="word-break:break-all;">
    <thead>
    <tr>
        <th>ID</th>
        <th>昵称</th>
        <th>商铺名称</th>
        <th>产品名称</th>
        <th>下单时间</th>
        <th>核销时间</th>
        <th>订单价格</th>
    </tr>
    </thead>
    <tbody>

    @if (!$orders)
        <tr>
            <td colspan="20">
                <div class="empty">暂无已核销订单记录!</div>
            </td>
        </tr>
    @endif
    @foreach($orders as $order)
        <tr>
            <td>{{ $order['id'] }}</td>
            <td>{{ $order['nickname'] }}</td>
            <td>{{ $order['title'] }}</td>
            <td>
                @if (empty($goods[$order['id']]))
                    该商品已被删除!
                @else
                    @foreach ($goods[$order['id']] as $item)
                        {{ $item['name'] }} <br>
                    @endforeach
                @endif

            </td>
            <td>{{ $order['pay_time'] }}</td>
            <td>{{ $order['verification_time'] }}</td>
            <td>{{ $order['price'] }}</td>
{{--            <td><a class="btn btn-default btn-sm" href="{{ route('admin.stats.sales.detail', ['id' => $order['id']]) }}" target="_blank">详情</a></td>--}}
        </tr>
    @endforeach
    </tbody>
</table>


@if ($orders)
    <div class="box-footer clearfix">
        @include('tool.paginator', ['paginator' => $paginator])
    </div>
@endif
