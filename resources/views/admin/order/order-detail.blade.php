<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">订单信息</h3>
        <div class="box-tools pull-right">
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body" style="display: block;">
        <div class="form-horizontal" style="float: left; width: 50%">
            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label>订单号</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control js-order-sn" data-order-sn="{{ $order['order_sn'] }}">{{ $order['order_sn'] }}</label>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label>买家昵称</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control">{{ $order['nickname'] }}</label>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label>下单时间</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control">{{ $order['pay_time'] }}</label>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label>流水号</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control">{{ $order['pay_order_sn'] }}</label>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label for="truename">联系人</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control">{{ $address['name'] ?? '' }}</label>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label for="truename">联系方式</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control">{{ $address['mobile'] ?? '' }}</label>
                </div>
            </div>

            @if ($order['status'] == 4)

            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label for="truename">售后备注</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control">
                            {{ $order['remark'] }}
                    </label>
                </div>
            </div>
            @endif


        </div>


        <div class="form-horizontal" style="float: left; width: 50%">
            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label for="truename">订单价格</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control">{{ $order['price'] }}</label>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label for="truename">订单状态</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control">{{ $status[$order['status']] }}</label>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label for="truename">订单创建时间</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control">{{ $order['created_at'] }}</label>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label for="truename">优惠</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control">
                        @if(!empty($coupon))
                            @if ($coupon['type'])
                                折扣({{ $coupon['discount'] }}%)
                            @else
                                {{ $coupon['price'] }}
                            @endif
                        @else
                            无
                        @endif
                    </label>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label for="truename">同创币抵扣</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control">
                        @if (empty($order['tongchuang']))
                            0
                        @else
                            {{ $order['tongchuang'] }}
                        @endif
                    </label>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-3 col-md-3 col-lg-3 control-label">
                    <label for="truename">收货地址</label>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 controls">
                    <label class="form-control">
                        @if (!empty($address))
                            {{ $address['province'].$address['city'].$address['area'].$address['address'] }}
                        @endif
                    </label>
                </div>
            </div>
        </div>


    </div><!-- /.box-body -->
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">产品信息</h3>
        <div class="box-tools pull-right">
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body" style="display: block;">

        <table class="table table-striped table-hover" style="word-break:break-all;">
            <thead>
            <tr>
                <th  style="width: 5%">ID</th>
                <th  style="width: 20%">商品名称</th>
                <th style="width: 40%">商品缩略图</th>
                <th>单价</th>
                <th>数量</th>
                <th>总价</th>
            </tr>
            </thead>
            <tbody>

            @foreach($products as $item)
                <tr>
                    <td>{{ $item['id'] }}</td>
                    <td>{{ $item['name'] }}</td>
                    <td><img src="/storage/{{ $item['thumbnail'] }}" style="width: 20%"></td>
                    <td>
                        @if ($type ?? 0)
                            {{ $item['price'] }}
                        @else
                            {{ $item['price'] }}
                        @endif
                    </td>
                    <td>{{ $item['total'] }}</td>
                    <td>{{ $item['total_price'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div><!-- /.box-body -->
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">物流</h3>
        <div class="box-tools pull-right">
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body" style="display: block;">

            <div class="form-inline well well-sm">
                <div>
                    <label for="tracking_name" style="margin-right: 5px">快递公司</label>
                    <div class="form-group">
                        <select class="form-control js-name" name="tracking_name">
                            <option value="">请选择快递公司</option>
                            @foreach ($ships as $key => $name)
                                <option @if($key == $order['tracking_name']) selected @endif value="{{ $key }}" >{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <label for="tracking_sn" style="margin-right: 5px">快递单号</label>
                    <div class="form-group">
                        <input class="form-control js-sn" type="text" placeholder="请输入快递单号" name="tracking_sn" value="{{ $order['tracking_sn'] ?? '' }}" />
                    </div>
                    <div class="form-group" style="margin-top: 5px">
                        @if ($order['status'] == 1)
                            <button class="btn btn-primary js-btn" type="submit" style="margin-left: 20px">添加快递单号</button>
                        @else
                            <button class="btn btn-primary js-inquiry" type="submit" style="margin-left: 20px">快递查询</button>
                        @endif
                    </div>
<script>
    $('.js-btn').on('click', function (res) {
        var tracking_sn = $('.js-sn').val()
        var tracking_name = $('.js-name').val()

        if(tracking_sn && tracking_name) {
            alert('确认添加快递单号' + tracking_sn )
        } else {
            alert('快递单号或快递名称不可为空!')
        }
        $.post('', {tracking_sn: tracking_sn, tracking_name: tracking_name}, function (res) {
            alert('添加成功!')
            location.reload();
        }).fail(function(response) {
            console.log(response.responseJSON.message)
            alert(response.responseJSON.message)
        });
    })
    $('.js-inquiry').on('click', function (res) {
        var tracking_sn = $('.js-sn').val()
        var tracking_name = $('.js-name').val()

        if(!tracking_sn || !tracking_name) {
            alert('快递单号或快递名称不可为空!')
        }
        var url = "https://www.kuaidi100.com/chaxun?com=" + tracking_name + "&nu=" + tracking_sn
        window.open(url);
    })
</script>
                </div>
            </div>

    </div><!-- /.box-body -->
</div>
responseJSON
