<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    @yield('title')
    <link rel="stylesheet" href="/assets/libs/layui/css/layui.css"/>
    <link rel="stylesheet" href="/assets/module/admin.css?v=318"/>
    <script type="text/javascript" src="/assets/libs/layui/layui.js"></script>
    <script type="text/javascript" src="/assets/js/common.js?v=318"></script>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
</head>

<body class="theme-pink" style="@yield('body-style', 'margin: 18px;')">

@yield('content')

@if (empty($display))
    @include('footer')
@endif

@yield('script')
@yield('style')
</body>
</html>
