<div class="form-group ">
    @if (in_array(1, $type) )
        <label for="startDate">核销时间</label>
    @endif
    @if (in_array(2, $type))
        <input class="form-control datetimepicker-input" type="text" id="startTime" name="startTime" value="{{ $dateRange['startTime'] }}" placeholder="起始日期">
        <label for="endDate">-</label>
        <input class="form-control datetimepicker-input" type="text" id="endTime" name="endTime" value="{{ $dateRange['endTime'] }}" placeholder="截至日期">
    @endif
    @if (in_array(3, $type))
        <button
            class="btn btn-default btn-data-range  @if (request()->get('date-range') == 'day') active @endif"
            type="button" id="btn-yesterday-range" data-type="day" data-start="{{ $dateRange['yesterdayStart'] }}"
            data-end="{{ $dateRange['yesterdayEnd'] }}">昨天</button>
        <button
            class="btn btn-default btn-data-range  @if (request()->get('date-range') == 'week') active @endif"
            type="button" id="btn-lastWeek-range" data-type="week" data-start="{{ $dateRange['lastWeekStart'] }}"
            data-end="{{ $dateRange['lastWeekEnd'] }}">最近七天</button>
        <button
            class="btn btn-default btn-data-range @if (request()->get('date-range') == 'month') active @endif"
            type="button" id="btn-lastMonth-range" data-type="month" data-start="{{ $dateRange['lastMonthStart'] }}"
            data-end="{{ $dateRange['lastMonthEnd'] }}">最近30天</button>
        <input id="hidden-data-range" name="date-range" type="hidden" value="{{ request()->get('date-range', 'week') }}">
    @endif

</div>

<script>
    var $startTime = $('#startTime');
    var $endTime = $("#endTime");
    $startTime.datetimepicker({
        format: "YYYY-MM-DD",
        locale: "zh-CN"
    }).on('changeDate', function () {
        $endTime.datetimepicker('setStartDate', $startTime.val());
        $endTime.datetimepicker('setEndDate', nextYear($startTime.val()));
    });

    $startTime.datetimepicker('setEndDate', $endTime.val());

    $endTime.datetimepicker({
        format: "YYYY-MM-DD",
        locale: "zh-CN"
    }).on('changeDate', function () {
        $startTime.datetimepicker('setEndDate', $endTime.val());
        $startTime.datetimepicker('setStartDate', prevYear($endTime.val()));
    });

    $endTime.datetimepicker('setStartDate', $startTime.val());

    $(".btn-data-range").on('click', function (res) {
        $('#hidden-data-range').attr('value', $(this).data('type'));
        $startTime.val($(this).data('start'))
        $endTime.val( $(this).data('end'))
        $('.js-form').submit();
    });
</script>
