<span class="text-muted" style="float: left">

            从 <b>{{ ($paginator->getCurrentPage() - 1) * $paginator->getPerPageCount() + 1 }}</b>
            到
            <b>
            @if ($paginator->getCurrentPage() == $paginator->getLastPage())
                    {{ $paginator->getItemCount() }}
                @else
                    {{ $paginator->getCurrentPage() * $paginator->getPerPageCount() }}
                @endif
            </b>
            ，总共 <b>{{ $paginator->getItemCount() }} </b>条

</span>

@if ($paginator ?? '' and $paginator->getLastPage() > 1)
    <nav >
        <ul class="pagination cd-pagination" style="float: right">
            @if ($paginator->getCurrentPage() != $paginator->getFirstPage())
                <li><a href="{{ $paginator->getPageUrl($paginator->getFirstPage()) }}"><i class="cd-icon cd-icon-first-page"></i> << </a></li>
                <li><a href="{{ $paginator->getPageUrl($paginator->getPreviousPage()) }}"><i class="cd-icon cd-icon-arrow-left"></i> < </a></li>
            @endif

            @foreach ($paginator->getPages() as $page)
                <li @if ($page == $paginator->getCurrentPage() )class="active" @endif><a  href="{{ $paginator->getPageUrl($page) }}">{{ $page }}</a></li>
            @endforeach

            @if ($paginator->getCurrentPage() != $paginator->getLastPage())
                <li><a href="{{ $paginator->getPageUrl($paginator->getNextPage()) }}"><i class="cd-icon cd-icon-arrow-right"></i> > </a></li>
                <li><a href="{{ $paginator->getPageUrl($paginator->getLastPage()) }}"><i class="cd-icon cd-icon-last-page"></i> >> </a></li>
            @endif
        </ul>
    </nav>
@endif

