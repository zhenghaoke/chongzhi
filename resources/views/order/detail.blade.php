@extends('layout', ['display' => true])

@section('title')
    <title>订单支付</title>
@endsection

@section('body-style')
@endsection

@section('content')
    <div class="header_sub">
        <ul class="layui-nav">
            <li class="layui-nav-item left">
                <a ew-event="back" title="上一页">
                    <i style="color: #007DDB;" class="layui-icon layui-icon-return"></i>
                </a>
            </li>
            <li class="layui-nav-item">
                订单支付
            </li>
            <li class="layui-nav-item right">
                <a>
                    <i style="float:right;color: white;" class="layui-icon">&#xe66f;</i>
                </a>
            </li>
        </ul>
    </div>

    <div class="layui-card-body">
        <table class="layui-table layui-text">
            <colgroup>
                <col width="90">
                <col>
            </colgroup>
            <tbody>
            <tr ew-tpl-rs="">
                <td>订单号</td>
                <td>{{ $order['order_sn'] }}</td>
            </tr>
            <tr>
                <td>手机号</td>
                <td style="color:red;font-size:25px;">{{ $order['phone'] }}</td>
            </tr>

            <tr>
                <td>订单名称</td>
                <td>{{ $order['name'] }}</td>
            </tr>
            <tr>
                <td>订单说明</td>
                <td style="color:red;">{{ $order['product']['detail'] }}</td>
            </tr>
            <tr>
                <td>订单金额</td>
                <td>{{ $order['settlement_price'] }}元</td>
            </tr>
            </tbody>
        </table>
        <br>
        <button style="background-color:#01BC0D;" type="button" class="layui-btn layui-btn-fluid">微信支付</button>
    </div>
@endsection

