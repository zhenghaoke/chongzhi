@extends('layout', ['display' => true])

@section('title')
    <title>会员折扣</title>
@endsection()

@section('content')


	<div class="header_sub">
		<ul class="layui-nav">
			<li class="layui-nav-item left">
				<a ew-event="back" title="上一页">
					<i style="color: #007DDB;" class="layui-icon layui-icon-return"></i>
				</a>
			</li>
			<li class="layui-nav-item">
				会员折扣
			</li>
			<li class="layui-nav-item right">
				<a>
					<i style="float:right;color: white;" class="layui-icon">&#xe66f;</i>
				</a>
			</li>
		</ul>
	</div>

	<div class="layui-card-body" style="padding: 25px;">
	                    <div class="text-center layui-text">
	                        <div class="user-info-head" id="userInfoHead">
	                            <img src="{{ $member['avatar'] }}" alt="">
	                        </div>
	                        <h2 style="padding-top: 20px;">{{ $member['nickname'] }}</h2>
	                    </div>
	</div>

	<div class="layui-col-md4 layui-col-sm5">
		<table class="layui-table" lay-skin="line" style="margin-top: 15px;">
			<thead>
			<tr>
				<td>等级</td>
				<td>累计充值</td>
				<td>折扣</td>
			</tr>
			</thead>
			<tbody>
            @foreach($levels as $level)
                <tr>
                    <td><span class="layui-badge layui-badge-green">VIP{{ $level['order'] }}</span></td>
                    <td>{{ $level['price'] }}</td>
                    <td>{{ $level['discount'] / 10 }}折</td>
                </tr>
            @endforeach
			</tbody>
		</table>
	</div>
    @endsection
