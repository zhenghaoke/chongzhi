@extends('layout')

@section('title')
    <title>个人中心</title>
@endsection()

@section('content')
    <div class="layui-row layui-col-space15">
        <div class="layui-col-xs12 layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-body">
                    <div class="user-info-head" id="userInfoHead">
                        <img src="{{ $member['avatar'] }}" alt="">
                        &nbsp;{{ $member['nickname'] }}
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-col-xs12 layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    我的提成<span class="layui-badge layui-badge-green pull-right">日</span>
                </div>
                <div class="layui-card-body">

                    <h1 class="lay-big-font" id="AnimNum1">￥23353</h1>
                    <p>总提成<span id="AnimNum2" class="pull-right">￥280888</span></p>
                </div>
            </div>
        </div>
    </div>

    <div class="layui-row layui-col-space15">
        <div class="layui-col-sm6" style="padding-bottom: 0;">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-xs6 layui-col-sm3">
                    <div class="console-app-group" ew-href="#" ew-title="代理">
                        <i class="console-app-icon layui-icon layui-icon-group" style="font-size: 26px;padding-top: 3px;margin-right: 6px;"></i>
                        <div class="console-app-name">代理</div>
                    </div>
                </div>
                <div class="layui-col-xs6 layui-col-sm3">
                    <div class="console-app-group">
                        <i class="console-app-icon layui-icon layui-icon-form" style="color: #b37feb;font-size: 30px;"></i>
                        <div class="console-app-name">订单</div>
                    </div>
                </div>
                <div class="layui-col-xs6 layui-col-sm3 js-member-vip"  data-url="{{ route('user.level') }}">
                    <div class="console-app-group">
                        <i class="console-app-icon layui-icon layui-icon-diamond" style="color: #de0003;"></i>
                        <div class="console-app-name">会员</div>
                    </div>
                </div>
                <div class="layui-col-xs6 layui-col-sm3">
                    <div class="console-app-group">
                        <i class="console-app-icon layui-icon layui-icon-rmb" style="color: #ff7ce7;"></i>
                        <div class="console-app-name">提现</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br><br><br><br><br><!-- 别删这个换行符 -->
@endsection()

@section('script')
    <script>
        layui.use(['admin','layer','form','notice'], function () {
            var $ = layui.jquery;
            var layer = layui.layer;
            var form = layui.form;
            var admin = layui.admin;
            var notice = layui.notice;

            admin.util.animateNum('#AnimNum1');
            admin.util.animateNum('#AnimNum2');

            $.get('member/notice',{},function(data){
                if(data.code == '200'){
                    console.log(data);
                    notice.success({
                        title: '公告',
                        message: data.notice
                    });
                    return;
                }
            })

            $('.js-member-vip').on('click', function (res) {
                window.location.href = $(this).data('url')
            })
        });
    </script>
@endsection()
