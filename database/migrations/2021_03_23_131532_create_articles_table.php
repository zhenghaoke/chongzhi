<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('标题');
            $table->string('desc')->comment('简介');
            $table->longText('content')->comment('正文');
            $table->integer('article_category_id')->comment('文章分类');
            $table->text('thumbnail')->comment('缩略图');
            $table->integer('type')->comment('文章类型 0集团介绍  1商城  2金融')->nullable();
            $table->integer('visited')->comment('访问量')->nullable()->default(0);
            $table->integer('shared')->comment('分享次数')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
