<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('联系人');
            $table->string('mobile')->comment('联系方式');
            $table->string('province')->comment('省');
            $table->string('city')->comment('市');
            $table->string('area')->comment('区');
            $table->string('address')->comment('详细地址');
            $table->integer('default')->comment('是否为默认   0 否 1 默认')->nullable()->default(0);
            $table->integer('member_id')->comment('用户id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address');
    }
}
