<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('公告标题');
            $table->text('content')->comment('公告内容');
            $table->integer('is_enable')->comment('是否启用  0 不启用 1 启用')->nullable()->default(0);
            $table->integer('order')->comment('排序');
            $table->date('start')->comment('开始日期')->nullable();
            $table->date('end')->comment('结束日期')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements');
    }
}
