<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeckillOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seckill_order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seckill_order_id')->comment('秒杀订单id');
            $table->integer('seckill_product_id')->comment('秒杀产品id');
            $table->float('price')->comment('价格');
            $table->integer('total')->comment('数量');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seckill_order_items');
    }
}
