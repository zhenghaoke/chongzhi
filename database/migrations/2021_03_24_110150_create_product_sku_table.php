<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSkuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_sku', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->comment('商品id');
            $table->string('content')->comment('sku内容');
            $table->string('tag')->comment('标签')->nullable();
            $table->integer('sku_id')->comment('sku内容 对应的sku_id');
            $table->string('sku_ids')->comment('sku内容 对应的sku_id')->nullable();
            $table->string('stock')->comment('剩余库存')->default(0)->nullable();
            $table->float('denomination')->comment('面值')->default(0)->nullable();
            $table->float('cost')->comment('成本')->default(0)->nullable();
            $table->float('price')->comment('单价')->default(0)->nullable();
            $table->integer('day')->comment('限制时间(天)')->default(0)->nullable();
            $table->integer('num')->comment('限制购买次数')->default(0)->nullable();
            $table->float('is_join')->comment('是否参与折扣')->default(0)->nullable();
            $table->integer('order')->comment('排序')->default(100)->nullable();
            $table->integer('is_enable')->comment('是否启用')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_sku');
    }
}
