<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('content')->comment('内容')->nullable();
            $table->string('type')->comment('类型  0 君泰币 1 同创币')->nullable()->default(0);
            $table->string('num')->comment('积分');
            $table->string('member_id')->comment('用户id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coin_log');
    }
}
