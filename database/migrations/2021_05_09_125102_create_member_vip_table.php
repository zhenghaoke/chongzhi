<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberVipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_vip', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('vip名称');
            $table->integer('order')->unique()->comment('vip等级');
            $table->float('price')->comment('价格阈值');
            $table->integer('discount')->comment('优惠(%)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_vip');
    }
}
