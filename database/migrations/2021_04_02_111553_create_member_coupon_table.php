<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_coupon', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_id')->comment('优惠券id');
            $table->string('member_id')->comment('用户id');
            $table->integer('expired')->comment('是否过期 0 未过期  1 已过期')->nullable()->default(0);
            $table->integer('use')->comment('是否已使用  0 未使用  1 已使用')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_coupon');
    }
}
