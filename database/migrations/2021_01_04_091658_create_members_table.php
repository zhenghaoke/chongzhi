<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nickname')->comment('会员昵称');
            $table->string('avatar')->comment('会员头像');
            $table->string('mobile')->comment('联系电话');
            $table->string('openid')->comment('微信openid');
            $table->integer('status')->comment('会员状态 0否 1 是')->nullable()->default(0);
            $table->integer('disable')->comment('用户状态 0否 1 是')->nullable()->default(0);
            $table->integer('vip_level')->comment('会员等级')->nullable()->default(0);
            $table->integer('spread_level')->comment('推广权限')->nullable()->default(0);
            $table->string('ip')->comment('登录ip')->nullable()->default(0);
            $table->float('balance')->comment('余额')->nullable()->default(0);
            $table->dateTime('login_time')->comment('登陆时间')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
