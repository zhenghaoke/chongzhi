<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_sn', 100)->comment('订单号');
            $table->string('pay_order_sn', 100)->comment('支付订单号')->nullable();
            $table->integer('member_id')->comment('用户id');
            $table->integer('product_id')->comment('产品id');
            $table->string('phone')->comment('充值手机号');
            $table->string('attribution')->comment('归属地')->nullable();
            $table->float('settlement_price')->comment('结算价');
            $table->integer('status')->comment('订单状态 0 待付款 1 待发货  2 待收货  3 待评价 4 已完成  5 退款  -1 已取消');
            $table->string('remark')->comment('备注')->nullable();
            $table->dateTime('pay_time')->comment('下单时间')->nullable();
            $table->dateTime('complete_time')->comment('完成时间')->nullable();
            $table->string('passage')->comment('通道')->nullable();
            $table->integer('pay_type')->comment('支付方式 0 微信 1 支付宝')->default(0);
            $table->integer('trashed')->comment('是否软删除')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
