<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('名称');
            $table->string('type')->comment('优惠券类型   0满减  1 折扣')->nullable();
            $table->integer('full')->comment('起用价格')->nullable();
            $table->integer('price')->comment('抵扣价格')->nullable();
            $table->integer('discount')->comment('折扣')->nullable();
            $table->date('start');
            $table->date('end');
            $table->integer('total')->comment('发放数量')->default(0)->nullable();
            $table->string('is_enable');
            $table->integer('applicable_type')->comment('使用类型  0 自选商品  1 自选商品分类')->nullable();
            $table->text('product_category_ids')->comment('自选商品分类id组')->nullable();
            $table->text('product_ids')->comment('自选商品id组')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon');
    }
}
