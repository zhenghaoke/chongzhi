<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('产品名称');
            $table->integer('product_category_id')->comment('产品分类id')->nullable();
            $table->integer('product_type')->comment('类型 1 自动  2 手动  3 活动');
            $table->integer('type')->comment('业务类型 1 话费  2 流量');
            $table->string('tag')->comment('标签');
            $table->longText('detail')->comment('详情');
            $table->float('price')->comment('价格');
            $table->string('operator')->comment('运营商 0电信  1移动  2联通');
            $table->longText('allow_area_ids')->comment('允许充值地区')->nullable();
            $table->longText('prohibit_area_ids')->comment('禁止充值地区')->nullable();
            $table->integer('num')->comment('验证码使用次数')->nullable()->default(1);
            $table->integer('sales')->comment('销售量')->nullable()->default(0);
            $table->longText('sku_ids')->comment('商品sku')->nullable();
            $table->integer('is_enable')->comment('是否上架')->default(0)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
