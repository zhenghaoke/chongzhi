<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_sn')->comment('订单号');
            $table->string('name')->comment('联系人');
            $table->string('mobile')->comment('联系方式');
            $table->string('province')->comment('收货地址');
            $table->string('city')->comment('收货地址');
            $table->string('area')->comment('收货地址');
            $table->string('address')->comment('收货地址');
            $table->string('address_id')->comment('地址id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_address');
    }
}
