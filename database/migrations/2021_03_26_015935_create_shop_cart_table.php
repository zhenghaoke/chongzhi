<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_cart', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->comment('产品id');
            $table->integer('product_sku_id')->comment('产品规格id');
            $table->integer('member_id')->comment('用户id');
            $table->integer('total')->comment('数量');
            $table->float('price')->comment('总价');
            $table->integer('is_check')->comment('是否选中')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_cart');
    }
}
