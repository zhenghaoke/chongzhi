<?php

use Illuminate\Database\Seeder;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'app_id' => 'wxb6b59af9ad4a9754',
                'secret' => '6610IeG45pskiSO7dQR6c6p0nMNy8Ox5',
                'mch_id' => '1596131451',
                'key' => 'xiabing5201314yeping199103141988',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];

        DB::table('configs')->insert($data);
    }
}
