<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 13,
                'parent_id' => 0,
                'order' => 1,
                'title' => '会员管理',
                'icon' => 'fa-bars',
                'uri' => 'members',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 14,
                'parent_id' => 0,
                'order' => 2,
                'title' => '会员卡管理',
                'icon' => 'fa-align-justify',
                'uri' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 15,
                'parent_id' => 14,
                'order' => 3,
                'title' => '订单管理',
                'icon' => 'fa-align-justify',
                'uri' => 'card_orders',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 16,
                'parent_id' => 14,
                'order' => 4,
                'title' => '设置',
                'icon' => 'fa-align-justify',
                'uri' => 'card_orders',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];

        DB::table('admin_menu')->insert($data);
    }
}
