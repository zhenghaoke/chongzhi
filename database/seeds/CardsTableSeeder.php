<?php

use Illuminate\Database\Seeder;

class CardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'price' => '30',
                'time_limit' => '31',
                'discount' => '50',
                'url' => '/card_img.png',
                'name' => '月卡',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'price' => '520',
                'time_limit' => '-1',
                'discount' => '50',
                'url' => '/card_img.png',
                'name' => '终生卡',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];
        DB::table('cards')->insert($data);
    }
}
