<?php

use Illuminate\Database\Seeder;

class BannerImgsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'is_open' => 1,
                'sort' => 0,
                'name' => '默认图片',
                'image' => '/banner_img.png',
//                'images' => json_encode(["images\/banner1.jpg","images\/banner2.jpg","images\/banner3.jpg"]),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];
        DB::table('banner_imgs')->insert($data);
    }
}
