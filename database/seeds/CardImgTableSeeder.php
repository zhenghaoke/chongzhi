<?php

use Illuminate\Database\Seeder;

class CardImgTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('card_img')->delete(1);
        $data = [
            [
                'id' => 1,
                'name' => '默认图片',
                'url' => '/card_img.png',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];
        DB::table('card_img')->insert($data);
    }
}
