<?php

namespace App\Tool;

use App\Models\Configs;
use EasyWeChat\Factory;
use EasyWeChat\MiniProgram\Application;

class MiniProgramSingleton
{
    private static $miniProgram;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function instantiate()
    {
        $config = self::getConfig();
        if (!(self::$miniProgram instanceof Application)) {
            self::$miniProgram = Factory::miniProgram($config);
        }

        return self::$miniProgram;
    }

    public static function getConfig()
    {
        $config = [
            'app_id' => Configs::where('key', 'APP_ID')->value('value'),
            'secret' => Configs::where('key', 'SECRET')->value('value'),


            // 下面为可选项
            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
//            'response_type' => 'array',
//            'log' => [
//                'level' => 'debug',
//                'file' => __DIR__ . '/wechat.log',
//            ],
        ];

        return $config;
    }
}
