<?php

namespace App\Tool;

use App\Models\Configs;
use EasyWeChat\Factory;
use EasyWeChat\Payment\Application;
use Illuminate\Support\Facades\URL;

class WeChatPaySingleton
{
    private static $payment;
    private static $config;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function instantiate($newConfig = [])
    {

        $config = Configs::all()->get(1)->toArray();
        if (empty($config)) {
            admin_toastr('请先设置微信配置', 'error');
            return redirect('/admin/configs');
        }
        $config = [
            // 必要配置
            'app_id' => Configs::where('key', 'APP_ID')->value('value'),
            'mch_id' => Configs::where('key', 'MCH_ID')->value('value'),
            'key' => Configs::where('key', 'KEY')->value('value'),   // API 密钥

            // 如需使用敏感接口（如退款、发送红包等）需要配置 API 证书路径(登录商户平台下载 API 证书)
//            'cert_path' => 'path/to/your/cert.pem', // XXX: 绝对路径！！！！
//            'key_path' => 'path/to/your/key',      // XXX: 绝对路径！！！！

            'notify_url' => URL::previous().'/orders/notify',     // 你也可以在下单时单独设置来想覆盖它
            'sandbox' => true, // 设置为 false 或注释则关闭沙箱模式
        ];

        $config = array_merge($config, $newConfig);
        if (!(self::$payment instanceof Application)) {
            self::$payment = Factory::payment($config);
        }

        return self::$payment;
    }

    public static function getConfig()
    {
        return self::$config;
    }

    public static function setConfig($newConfig)
    {
        $config = [
            // 必要配置
            'app_id' => env('WECHAT_APP_ID'),
            'mch_id' => env('WECHAT_MCH_ID'),
            'key' => env('WECHAT_KEY'),   // API 密钥

            // 如需使用敏感接口（如退款、发送红包等）需要配置 API 证书路径(登录商户平台下载 API 证书)
//            'cert_path' => 'path/to/your/cert.pem', // XXX: 绝对路径！！！！
//            'key_path' => 'path/to/your/key',      // XXX: 绝对路径！！！！


            'notify_url' => URL::previous().'/orders/notify',     // 你也可以在下单时单独设置来想覆盖它
            'sandbox' => true, // 设置为 false 或注释则关闭沙箱模式
        ];

        self::$config = array_merge($config, $newConfig);
    }
}
