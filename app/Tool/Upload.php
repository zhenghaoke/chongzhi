<?php

namespace App\Tool;

use Illuminate\Http\Request;

class Upload
{
    public static function images(Request $request, $name)
    {
        $url = null;
        $file = $request->file($name);
        if (!empty($file)) {
            if (!in_array($file->getMimeType(), ['image/jpeg', 'image/png'])) {
                throw new \Exception('不支持以外jpg和png的图片格式!');
            }
            $url = $file->store('public/images/' . date('Y-m-d') . '/withdraw');
            $url = explode('/', $url);
            unset($url[0]);
            $url = join('/', $url);
        }

        return $url;
    }

    public static function appletsImage()
    {
        $file = $_FILES['file'];
        $Path = "/images/" . date('Y-m-d') . "/withdraw/";
        if (!empty($file)) {

            $type = pathinfo($file['name'])['extension'];
            if (!in_array($type, ['jpg', 'png', 'gif'])) {
                throw new \Exception('不支持以外jpg和png还有gif的图片格式!');
            }

            $fileName = request()->server('DOCUMENT_ROOT') . '/storage' . $Path;//文件路径
            $upload_name = 'img_' . date("YmdHis") . rand(0, 100) . '.' . $type;//文件名加后缀
            if (!file_exists($fileName)) {
                //进行文件创建
                mkdir($fileName, 0777, true);
            }
            $imageSavePath = $fileName . $upload_name;
            if (move_uploaded_file($_FILES['file']['tmp_name'], $imageSavePath)) {
                return $Path . $upload_name;
            }
        }

        return null;
    }
}
