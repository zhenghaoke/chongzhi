<?php

namespace App\Tool;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class Tool
{
    public static function getRemember(Request $request)
    {
        $keys = array_keys($request->cookie());
        $remember = '';
        foreach ($keys as $key) {
            if ('remember_web' == substr($key, 0, 12)) {
                $remember = $key;
                break;
            }
        }
        $remember = $request->cookie($remember);

        return explode('|', $remember);
    }

    public static function generateCode()
    {
        return date('mdHsi', time()) . mt_rand(100, 999);
    }

    public static function queryAddress($keyWords)
    {
        $header[] = 'Referer: http://lbs.qq.com/webservice_v1/guide-suggestion.html';
        $header[] = 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36';
        $url = "http://apis.map.qq.com/ws/place/v1/suggestion/?&region=&key=OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77&keyword=" . $keyWords;

        $ch = curl_init();
        //设置选项，包括URL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        //执行并获取HTML文档内容
        $output = curl_exec($ch);

        //释放curl句柄
        curl_close($ch);
        $result = json_decode($output, true);
        return $result;
        //echo json_encode(['error_code'=>'SUCCESS','reason'=>'查询成功','result'=>$result);
    }

    public static function getImgHtmlByUrl($urls, $width = '20%', $height = '20%')
    {
        if (!is_array($urls)) {
            $urls = [$urls];
        }
        $html = '';
        foreach ($urls as $url) {
            $html .= "<img src='/storage/{$url}' width='{$width}' height='{$height}'/><br/>";
        }

        return $html;
    }

    public static function generateSn($type = 0)
    {
        $prefix = $type ? 'MX' : 'SP';

        return $prefix . date('YmdHis', time()) . mt_rand(10000, 99999);
    }

    public static function validationCode()
    {
        return mt_rand(100000, 999999);
    }

    public static function ships()
    {
        return [
            'yunda' => '韵达',
            'shunfeng' => '顺丰',
            'yuantong' => '圆通',
            'zhongtong' => '中通',
            'shentong' => '申通',
        ];
    }

    public static function getAttribution($mobile)
    {
        if (!self::is_mobile($mobile)) {
            return false;
        }

        $client = new Client();
        $url = 'https://cx.shouji.360.cn/phonearea.php?number=' . $mobile;
        $response = $client->request('GET', $url);
        $result = $response->getBody()->getContents();
        $result = json_decode($result, true);

        return $result['data'];
    }

    public static function is_mobile($mobile)
    {
        $result = preg_match('/^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/', $mobile);
        return (bool)$result;
    }
}
