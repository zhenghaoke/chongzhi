<?php

namespace App\Dbu;

class AutoLoad
{
    protected $dbu;

    public function __construct(Dbu $dbu)
    {
        $this->dbu = $dbu;

        // 绑定生成service类到make.service上
        $dbu['make.service'] = function ($dbu) {
            // 返回一个创建service的闭包
            return function ($namespace, $classname) use ($dbu) {
                $class = $namespace . "\\" . $classname;
                return new $class($dbu);
            };
        };

        $dbu['make.model'] = function ($dbu) {
            return function ($namespace, $classname) use ($dbu) {
                $class = $namespace . "\\" . $classname;
                return new $class([], $dbu);
            };
        };
    }

    public function autoloads($slug, $type)
    {
        if (isset($this->dbu["@{$slug}{$type}"])) {
            return $this->dbu["@{$slug}{$type}"];
        }
        if ($type == 'model') {
            return $this->autoloadModel($slug);
        }

        return $this->autoloadService($slug);
    }

    private function autoloadService($slug)
    {
        $name = explode(':', $slug);
        if (count($name) == 1) {
            $namespace = "App\\Dbu\\Service";
        }

        if (count($name) == 2) {
            $namespace = "App\\Dbu\\Service\\{$name[0]}";
        }

        if (empty($namespace)) {
            throw  new \Exception('unrecognized $slug parameter !');
        }
        $classname = end($name) . "Service";

        // 获取创建service的方法
        $maker = $this->dbu['make.service'];
        $service = $maker($namespace, $classname);
        $this->dbu["@{$slug}service"] = $service;

        return $service;
    }

    private function autoloadModel($slug)
    {
        $name = explode(':', $slug);
        if (count($name) == 1) {
            $namespace = "App\\Models";
        }

        if (count($name) == 2) {
            $namespace = "App\\Models\\{$name[0]}";
        }

        if (empty($namespace)) {
            throw  new \Exception('unrecognized $slug parameter !');
        }
        $classname = end($name);

        // 获取创建service的方法
        $maker = $this->dbu['make.model'];
        $model = $maker($namespace, $classname);
        $this->dbu["@{$slug}model"] = $model;

        return $model;
    }

}
