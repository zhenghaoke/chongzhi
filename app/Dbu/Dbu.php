<?php

namespace App\Dbu;

use Pimple\Container;

class Dbu extends Container
{
    public function __construct(array $values = [])
    {
        parent::__construct($values);

        $dbu = $this;

        // 绑定自动加载类到make上
        $dbu['make'] = function ($dbu) {
            return new AutoLoad($dbu);
        };
    }

    public function model($slug)
    {
        return $this['make']->autoloads($slug, 'model');
    }

    public function service($slug)
    {
        return $this['make']->autoloads($slug, 'service');
    }
}
