<?php

namespace App\Dbu;

class BaseService
{
    private $dbu;

    public function __construct(Dbu $dbu)
    {
        $this->dbu = $dbu;
    }

    public function getDbu()
    {
        return $this->dbu;
    }
}
