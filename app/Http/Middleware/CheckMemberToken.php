<?php

namespace App\Http\Middleware;

use App\Models\Member\Members;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Closure;

class CheckMemberToken
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $token = JWTAuth::getToken();
            if (empty($token)) {
                return response()->json(['code' => 4001, 'message' => '未登录']);
            }

            $user_info = JWTAuth::setToken($token)->getPayload()->get('sub');
            if (!empty($user_info)) {
                $user = Members::where('mobile', $user_info->mobile)->first();
                if (!$user) {
                    response()->json(['code' => 4002, 'message' => '用户异常']);
                }

                //如果想向控制器里传入用户信息，将数据添加到$request里面
                $request->attributes->add(['memberId' => $user->id]); //添加参数
            }
            //其他地方获取用户值
//            var_dump($request->attributes->get('memberId'));exit();
            return $next($request);
        } catch (TokenExpiredException $e) {
            try {
                $token = JWTAuth::refresh();
                if ($token) {
                    return response()->json(['code' => 4003, 'message' => '新token', 'token' => $token]);
                }
            } catch (JWTException $e) {
                return response()->json(['code' => 4004, 'message' => 'token无效', 'token' => '']);
            }
        }
    }
}
