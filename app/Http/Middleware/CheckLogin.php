<?php

namespace App\Http\Middleware;

use App\Models\Member\Members;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param null $guard
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            throw new \Exception('您还未登录!');
        }
        if (auth()->user()->disable) {
            throw new \Exception('您已被封禁，无法登入！');
        }

        return $next($request);
    }
}
