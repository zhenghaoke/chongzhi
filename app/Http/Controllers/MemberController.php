<?php

namespace App\Http\Controllers;

use App\Dbu\Dbu;
use App\Models\Configs;
use App\Models\Member\Members;
use App\Models\Member\MemberVip;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function __construct(Dbu $dbu)
    {
        parent::__construct($dbu);
//        \Auth::guard();
//        $this->middleware('guest');
//        dd();
//        dd(\A-uth::guest(), auth()->user());
//        if (!\Auth::check()) {
//            throw new \Exception('您还未登录!');
//        }
    }

    public function index(Request $request)
    {
        return view('member.my', [
            'member' => \Auth::user(),
//            'member' => Members::find(1),
        ]);
    }

    public function memberLevel()
    {
        return view('member.vip-info', [
            'member' => Members::getMember(),
            'levels' => MemberVip::OrderBy('order')->get()->toArray(),
        ]);
    }

    public function info(Request $request)
    {
        return ['code' => 200, 'data' => Members::getMember()];
    }

    public function notice(Request $request)
    {
        return ['code' => 200, 'notice' => Configs::where('key', 'NOTICE')->value('value')];
    }

}
