<?php

namespace App\Http\Controllers;

use App\Models\MobileValidationCode;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    //接口地址
    const URL = 'http://sms.edmcn.cn/api/cm/trigger_mobile.php';
    const SUFFIX = '君泰同创科技';

    public function getCode(Request $request)
    {
        $mobile = $request->get('mobile');

        if (empty($mobile)) {
            return ['code' => 404, 'message' => '手机号不可为空!'];
        }
        if ($request->session()->get($mobile.'_submit') + 60 >= time()) {
            return ['code' => 404, 'message' => '提交过于频繁, 请稍后再试!'];
        }

        try {
          $result = MobileValidationCode::createValidationCode($mobile, self::SUFFIX, self::URL);
        }catch (\Exception $e) {
            return ['code' => 404, 'message' => $e->getMessage()];
        }
        $request->session()->put($mobile.'_submit', time());

        return ['code' => 200, 'message' => $result];
    }
}
