<?php

namespace App\Http\Controllers;

use App\Dbu\Dbu;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    private $dbu;
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(Dbu $dbu)
    {
        $this->dbu = $dbu;
    }

    public function getDbu()
    {
        return $this->dbu;
    }
}
