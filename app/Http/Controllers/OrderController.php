<?php

namespace App\Http\Controllers;

use App\Models\Order\Orders;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function detail(Request $request)
    {
        $orderSn = $request->query->get('ordersn');
        $order = Orders::getBySn($orderSn);

        return view('order.detail', [
            'order' => $order
        ]);
    }

    // 创建普通订单
    public function create(Request $request)
    {
        $type = $request->request->get('type');
        $id = $request->request->get('id');
        $mobile = $request->request->get('mobile', '18358775257');
        try {
            $orderSn = $this->getOrderModel()->createOrder($id, $type, $mobile);
        } catch (\Exception $e) {
            return ['code' => 404, 'massage' => $e->getMessage()];
        }

        return ['code' => 200, 'massage' => '创建成功', 'order_sn' => $orderSn];
    }


    // 确认普通订单页面
    public function confirmOrder(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $cartIds = $request->query->get('cart_ids');
        $type = $request->query->get('type', 1);
        $total = $request->query->get('total', 0);
        $addressId = $request->query->get('address_id', 0);
        $couponId = $request->query->get('coupon_id', 0);

        $data = Orders::confirmOrder($memberId, $cartIds, $type, $addressId, $couponId, $total);

        return ['code' => 200, 'data' => $data];
    }

    // 订单确认收货
    public function confirmReceipt(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $orderSn = $request->request->get('order_sn');

        $result = Orders::confirmReceipt($orderSn, $memberId);
        if (empty($result)) {
            return ['code' => 404, 'massage' => '操作失败!'];
        }

        return ['code' => 200, 'massage' => '操作成功!'];
    }

    // 删除订单
    public function delete(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $orderSn = $request->request->get('order_sn');

        $result = Orders::deleteOrder($orderSn, $memberId);
        if (empty($result)) {
            return ['code' => 404, 'massage' => '操作失败!'];
        }

        return ['code' => 200, 'massage' => '操作成功!'];
    }

    // 删除订单
    public function remind(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $orderSn = $request->request->get('order_sn');

        $result = Orders::remind($orderSn, $memberId);
        if (empty($result)) {
            return ['code' => 404, 'massage' => '操作失败!'];
        }

        return ['code' => 200, 'massage' => '操作成功!'];
    }

    // 订单售后
    public function afterSale(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $orderSn = $request->request->get('order_sn');

        $result = Orders::afterSale($orderSn, $memberId);
        if (empty($result)) {
            return ['code' => 404, 'massage' => '操作失败!'];
        }

        return ['code' => 200, 'massage' => '操作成功!'];
    }

    // 搜索订单
    public function searchOrders(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $status = $request->query->get('status', 'all');
        $page = $request->query->get('page', 1);

        $data = Orders::searchOrders($status, $memberId, $page);

        return ['code' => 200, 'data' => $data];
    }

    // 订单详情
    public function orderDetail(Request $request)
    {
        $orderSn = $request->query->get('order_sn');

        try {
            $data = Orders::orderDetail($orderSn);
        } catch (\Exception $e) {
            return ['code' => 404, 'message' => $e->getMessage()];
        };

        return ['code' => 200, 'data' => $data];
    }

    /**
     * @return Orders
     */
    protected function getOrderModel()
    {
        return $this->getDbu()->model('Order:Orders');
    }
}
