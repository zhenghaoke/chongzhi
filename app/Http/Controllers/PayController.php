<?php

namespace App\Http\Controllers;

use App\Models\Pay;
use Illuminate\Http\Request;

class PayController extends Controller
{
    public function payByWechat(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $orderSn = $request->get('orderSn');
        $type = $request->get('type', 'base');
        try {
            return ['code' => 200, 'data' => (new Pay)->payByWeChat($orderSn, $memberId, $type)];
        } catch (\Exception $e) {
            return ['code' => 404, 'massage' => $e->getMessage()];
        }
    }

    public function notifyByWechat()
    {
        return Pay::notifyByWeChat();
    }

}
