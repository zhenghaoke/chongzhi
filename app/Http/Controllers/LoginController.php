<?php

namespace App\Http\Controllers;

use App\Models\Member\Members;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class LoginController extends Controller
{
    public function auth(Request $request)
    {
        $mobile = $request->request->get('mobile');
        $code = $request->request->get('code');

        try {
            $user = $this->getModel()->createMember($mobile, $code);
        }catch (\Exception $e) {
            return ['code' => 404, 'message' => $e->getMessage()];
        }

        $customClaims = ['sub' => ["mobile" => $user->mobile, 'nickname' => $user->nickname]];
        $payload = JWTFactory::customClaims($customClaims)->make();

        if (!$token = JWTAuth::encode($payload)->get()) {
            return ['code' => 401, 'massage' => 'Unauthorized'];
        }

        return ['code' => 200, 'token' => $token];
    }

    public function updateMobile(Request $request)
    {
        $mobile = $request->request->get('mobile');
        $code = $request->request->get('code');
        $memberId = $request->attributes->get('memberId');

        try {
            $user = $this->getModel()->updateMobile($memberId, $mobile, $code);
        }catch (\Exception $e) {
            return ['code' => 404, 'message' => $e->getMessage()];
        }
        if (empty($user)) {
            return ['code' => 404, 'message' => '修改失败!'];
        }

        $customClaims = ['sub' => ["mobile" => $user['mobile'], 'nickname' => $user['nickname']]];
        $payload = JWTFactory::customClaims($customClaims)->make();

        if (!$token = JWTAuth::encode($payload)->get()) {
            return ['code' => 401, 'massage' => 'Unauthorized'];
        }

        return ['code' => 200, 'token' => $token];
    }

    /**
     * @return Members
     */
    private function getModel()
    {
        return $this->getDbu()->model('Members');
    }
}
