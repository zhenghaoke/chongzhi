<?php

namespace App\Http\Controllers;

use App\Models\Announcements;
use App\Models\Article\Articles;
use App\Models\Banner;
use App\Models\Product\Products;
use App\Models\Recommends;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function error(Request $request)
    {
        return view('error', $request->all());
    }

    // 行情动态
    public function news(Request $request)
    {
        $page = $request->query->get('page', 1);
        $limit = 10;
        list($articles, $count) = Articles::getList($page, $limit);

        return ['code' => 200, 'articles' => $articles, 'count' => $count];
    }

    // banner图
    public function banners(Request $request)
    {
        return ['code' => 200, 'banners' => Banner::get()];
    }

    // 推荐
    public function recommends(Request $request)
    {
        $recProducts = Products::getRecommend();
        $rec = Recommends::get();
        return ['code' => 200, 'rec' => $rec, 'recProducts' => $recProducts,];
    }
}
