<?php

namespace App\Http\Controllers;

use App\Models\CoinLogs;
use Illuminate\Http\Request;

class CoinLogsController extends Controller
{
    public function index(Request $request)
    {
        $page = $request->query->get('page',1);
        $memberId = $request->attributes->get('memberId');

        return ['code' => 200, 'data' => CoinLogs::getLogs($page, $memberId)];
    }

    public function juntai(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        return ['code' => 200, 'num' => CoinLogs::countJuntaiNum($memberId)];
    }

    public function tongchuang(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        return ['code' => 200, 'num' => CoinLogs::countTongchuangNum($memberId)];
    }

    public function juntaiLogs(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        return ['code' => 200, 'num' => CoinLogs::juntaiLogs($memberId)];
    }

    public function tongchuangLogs(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        return ['code' => 200, 'num' => CoinLogs::tongchuangLogs($memberId)];
    }

    public function balanceLogs(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        return ['code' => 200, 'num' => CoinLogs::balanceLogs($memberId)];
    }

}
