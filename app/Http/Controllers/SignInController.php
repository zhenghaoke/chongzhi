<?php

namespace App\Http\Controllers;

use App\Models\CoinLogs;
use App\Models\SignIn;
use Illuminate\Http\Request;

class SignInController extends Controller
{
    public function getLogs(Request $request)
    {
        $page = $request->query->get('page',1);
        $memberId = $request->attributes->get('memberId');

        return ['code' => 200, 'data' => CoinLogs::getSignInLogs($page, $memberId)];
    }

    // 签到功能
    public function signIn(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        try {
            $num = (new SignIn)->signIn($memberId);
            return ['code' => 200, 'message' => '签到成功', 'juntai' => $num];
        }catch (\Exception $e) {
            return ['code' => 404, 'message' => $e->getMessage(), 'juntai' => null];
        }
    }
}
