<?php

namespace App\Http\Controllers;

use App\Models\Product\Products;
use App\Models\Seckill\SeckillOrders;
use App\Models\Seckill\SeckillProducts;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    // 产品搜索
    public function index(Request $request)
    {
        $keyword = $request->query->get('keyword', '');
        $page = $request->query->get('page', 1);
        $products = Products::search($keyword, $page);

        return ['code' => 200, 'data' => $products];
    }

    // 产品列表
    public function list(Request $request)
    {
        $page = $request->query->get('page', 1);
        $id = $request->query->get('cate_id', 0);
        $products = Products::getByCategoryId($id, $page);

        return ['code' => 200, 'data' => $products];
    }

    // 根据分类id 获取 商品
    public function findByCategoryId(Request $request)
    {
        $id = $request->query->get('id');
        $page = $request->query->get('page', 1);

        try {
            $products = Products::findByCategoryId($id, $page);
        }catch (\Exception $e) {
            return ['code' => 200, 'message' => $e->getMessage()];
        }

        return ['code' => 200, 'data' => $products];
    }

    // 根据分类id 获取 秒杀商品
    public function findSeckillByCategoryId(Request $request, $id)
    {
        $page = $request->query->get('page', 1);
        $products = SeckillOrders::findByCategoryId($id, $page);

        return ['code' => 200, 'data' => $products];
    }

    // 产品详情
    public function detail(Request $request, $id)
    {
        try {
            $product = Products::productDetail($id);
        } catch (\Exception $e) {
            return ['code' => 200, 'message' => $e->getMessage()];
        }
        return ['code' => 200, 'data' => $product];
    }
}
