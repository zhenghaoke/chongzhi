<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Product\Products;
use App\Models\Seckill\SeckillOrders;
use Illuminate\Http\Request;

class PhoneBillController extends Controller
{
    public function index(Request $request)
    {
        $mobile = $request->query->get('mobile', '');
        $page = $request->query->get('page', 1);
        $limit = 10;
        $data = Products::getProducts($mobile, $page, $limit);

        return view('product.phone-bill', $data);
    }

    // 产品列表
    public function list(Request $request)
    {
        $mobile = $request->query->get('mobile', '');
        $data = Products::getProducts($mobile);
        $data['code'] = 200;

        return $data;
    }

    // 根据分类id 获取 商品
    public function findByCategoryId(Request $request)
    {
        $id = $request->query->get('id');
        $page = $request->query->get('page', 1);

        try {
            $products = Products::findByCategoryId($id, $page);
        }catch (\Exception $e) {
            return ['code' => 200, 'message' => $e->getMessage()];
        }

        return ['code' => 200, 'data' => $products];
    }

    // 根据分类id 获取 秒杀商品
    public function findSeckillByCategoryId(Request $request, $id)
    {
        $page = $request->query->get('page', 1);
        $products = SeckillOrders::findByCategoryId($id, $page);

        return ['code' => 200, 'data' => $products];
    }

    // 产品详情
    public function detail(Request $request, $id)
    {
        try {
            $product = Products::productDetail($id);
        } catch (\Exception $e) {
            return ['code' => 200, 'message' => $e->getMessage()];
        }
        return ['code' => 200, 'data' => $product];
    }
}
