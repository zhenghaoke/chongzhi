<?php

namespace App\Http\Controllers;

use App\Models\Article\Articles;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function detail(Request $request, $id)
    {
        $article = Articles::where('id', $id)->get()->first();
        return ['code' => 200, 'article' => empty($article) ? [] : $article->toArray()];
    }

    public function mall(Request $request)
    {
        $article = Articles::where('id', 3)->get()->toArray();
        return ['code' => 200, 'article' =>  empty($article) ? [] : reset($article)];
    }

    public function addVisited(Request $request)
    {
        $id = $request->request->get('id');

        try {
            $this->getModel()->addVisited($id);
        } catch (\Exception $e) {
            return ['code' => 404, 'message' => $e->getMessage()];
        }

        return ['code' => 200, 'message' => '成功'];
    }

    public function addShared(Request $request)
    {
        $id = $request->request->get('id');

        try {
            $this->getModel()->addShared($id);
        } catch (\Exception $e) {
            return ['code' => 404, 'message' => $e->getMessage()];
        }

        return ['code' => 200, 'message' => '成功'];
    }

    public function financial(Request $request)
    {
        $article = Articles::where('id', 4)->get()->toArray();
        return ['code' => 200, 'article' => empty($article) ? [] : reset($article)];
    }

    public function juntai(Request $request)
    {
        $article = Articles::where('id', 5)->get()->toArray();
        return ['code' => 200, 'article' => empty($article) ? [] : reset($article)];
    }

    public function tongchuang(Request $request)
    {
        $article = Articles::where('id', 6)->get()->toArray();
        return ['code' => 200, 'article' => empty($article) ? [] : reset($article)];
    }

    /**
     * @return Articles
     */
    private function getModel()
    {
        return $this->getDbu()->model('Article:Articles');
    }
}
