<?php

namespace App\Http\Controllers;

use App\Models\Configs;
use App\Models\Member\Members;
use App\Models\Shops;
use Auth;
use EasyWeChat\Factory;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class WechatController extends Controller
{
    public function auth(Request $request)
    {
        $code = $request->get('code');
        $rawData = $request->get('rawData', '');
        $bindCode = $request->get('bindCode', '');
        $verifier = $request->get('verifier', false);

        $config = [
            'app_id' => Configs::where('key', 'APP_ID')->value('value'),
            'secret' => Configs::where('key', 'SECRET')->value('value'),
        ];

        $app = Factory::miniProgram($config);
        $data = $app->auth->session($code);

        //判断code是否过期
        if (isset($data['errcode'])) {
            return ['code' => 404, 'massage' => 'code已过期或不正确'];
        }
        $weappOpenid = $data['openid'];
        $weixinSessionKey = $data['session_key'];

        if (!empty($rawData)) {
            $wechat = json_decode($rawData, true);
            $user = Members::UpdateOrCreate(['openid' => $weappOpenid], [
                'openid' => $weappOpenid,
                'nickname' => $wechat['nickName'],
//                'gender' => $wechat['gender'],
//                'city' => $wechat['city'], // 'Wenzhou',
//                'province' => $wechat['province'], // 'Zhejiang',
//                'country' => $wechat['country'], // 'China',
                'avatar' => $wechat['avatarUrl'],
                'session_key' => $weixinSessionKey,
                'mobile' => $wechat['mobile'] ?? '--',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            // 扫推广码
            if ($bindCode) {
                Members::bindShop($user->id, $bindCode);
            }
        }
        if (empty($rawData)) {
            $user = Members::where(['openid' => $weappOpenid])->get();
            if (empty($user)) {
                return ['code' => 404, 'massage' => '请通过小程序登录后重试!'];
            }
            $user = $user->toArray();
            $user = (object)reset($user);
        }

        $customClaims = ['sub' => ["openid" => $user->openid, 'session_key' => $user->session_key]];
        $payload = JWTFactory::customClaims($customClaims)->make();

        if (!$token = JWTAuth::encode($payload)->get()) {
            return ['code' => 401, 'massage' => 'Unauthorized'];
        }
//        $ttl = $request->out_time ?? config('jwt.ttl'); # 设置token 过期时间

//        if (!$token = Auth::guard('api')->setTTL($ttl)->tokenById($user->id)) {
//            return ['code' => 500, 'massage' => 'token 过期'];
//        }

//        return apiJson($this->respondWithToken($token));
        $shop = Shops::where('member_id', $user->id)->where('review', 1)->get()->toArray();
        $type = (int)!empty($shop);
        if (empty($shop)) {
            $shop = Shops::where('verifier_ids', 'like', '%"' . $user->id . '"%')->where('review', 1)->get()->toArray();
            $type = !empty($shop) ? 2 : $type;
        }

        if ($verifier && !in_array($type, [1, 2])) {
            return ['code' => 201, 'token' => $token, 'massage' => '您暂还没有核销权限, 请联系管理员获取权限后继续!'];
        }

        return ['code' => 200, 'token' => $token, 'type' => $type, 'shopId' => reset($shop)['id'] ?? false];
    }

    public function public(Request $request)
    {
        // 是否已登录
        if (Auth::check()) {
            if (auth()->user()->disable) {
                throw new \Exception('您已被封禁，无法登入！');
            }
            return redirect(route('user'));
        }
        $config = [
            'app_id' => Configs::where('key', 'APP_ID')->value('value'),
            'secret' => Configs::where('key', 'SECRET')->value('value'),
            'response_type' => 'array',
            'oauth' => [
                'scopes'   => ['snsapi_base'],
                'callback' => route('auth.get_oauth'),
            ],
        ];

        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;
        return $oauth->redirect();

    }

    public function getOAuth(Request $request)
    {
        $config = [
            'app_id' => Configs::where('key', 'APP_ID')->value('value'),
            'secret' => Configs::where('key', 'SECRET')->value('value'),
            'response_type' => 'array',
        ];

        $app = Factory::officialAccount($config);
        $user = $app->oauth->user()->toArray();
        $openid = $user['original']['openid'];

        $user = Members::where('openid', $openid)->get()->first();
        $isRegister = Configs::where('key', 'IS_REGISTER')->value('value');
        if (empty($isRegister) && empty($user)) {
            throw new \Exception('暂未开放注册功能！');
        }
        if (!empty($user) && $user->disable) {
            throw new \Exception('您已被封禁，无法登入！');
        }

        $user = $app->user->get($openid); // 获取用户信息
        $user = Members::UpdateOrCreate(['openid' => $openid], [
            'openid' => $openid,
            'nickname' => $user['nickname'],
//            'gender' => $user['gender'],
//            'city' => $user['city'], // 'Wenzhou',
//            'province' => $user['province'], // 'Zhejiang',
//            'country' => $user['country'], // 'China',
            'avatar' => $user['headimgurl'],
//            'subscribe_time' => $user['subscribe_time'],
            'mobile' => '--',
            'login_time' => date('Y-m-d H:i:s'),
        ]);

        \Auth::login($user);

        return redirect(route('user'));
    }

}
