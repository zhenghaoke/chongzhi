<?php

namespace App\Models;

use App\Models\Product\Products;
use App\Models\Product\ProductSku;
use App\Tool\ArrayTool;

class ShopCart extends Model
{
    protected $table = 'shop_cart';

    public function checkOne($id, $memberId)
    {
        $cartModel = self::where('id', $id)
            ->where('member_id', $memberId);
        $check = $cartModel->value('is_check');

        return $cartModel->update($this->timestamp(['is_check' => !$check], 1));
    }

    public static function hasAllCheck($memberId)
    {
        $checks = self::where('member_id', $memberId)->get(['is_check'])->toArray();
        $checks = empty($checks) ? [] : array_column($checks, 'is_check');
        $check = 0;
        if (array_sum($checks) == count($checks) && count($checks) != 0) {
            $check = 1;
        }

        return $check;
    }

    public function checkAll($memberId)
    {
        $cartModel = self::where('member_id', $memberId);
        $checks = $cartModel->get(['is_check'])->toArray();
        $checks = empty($checks) ? [] : array_column($checks, 'is_check');
        $check = 1;
        if (array_sum($checks) == count($checks) && count($checks) != 0) {
            $check = 0;
        }

        return $cartModel->update($this->timestamp(['is_check' => $check], 1));
    }

    public static function createOrUpdate($fields, $memberId)
    {
        $fields = ArrayTool::parts($fields, ['product_id', 'product_sku_id', 'total']);
        if (empty($fields['product_id']) || empty($fields['product_sku_id'])) {
            throw new \Exception('缺少产品id或skuid');
        }
        if (empty($fields['total'])) {
            throw new \Exception('缺少产品数量');
        }

        // 产品是否存在
        $sku = ProductSku::where('id', $fields['product_sku_id'])
            ->where('product_id', $fields['product_id'])
            ->where('stock', '>', (int)$fields['total'])
            ->get()->first();

        // 查询购物车中是否存在产品
        $cartModel = self::where('product_id', $fields['product_id'])
            ->where('product_sku_id', $fields['product_sku_id'])
            ->where('member_id', $memberId);
        $cart = $cartModel->get()->first();

        if (empty($sku) || !empty($cart) && $sku['stock'] < $cart['total'] + $fields['total']) {
            throw new \Exception('产品库存不足');
        }
        // 增加产品数量
        if (!empty($cart)) {
            $count = $cart['total'] + $fields['total'];
            if ($count > 0) {
                return $cartModel->update([
                    'total' => $count,
                    'price' => $sku['price'] * $count,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            } else {
                return $cartModel->delete();
            }
        }

        // 添加产品数量
        $fields['price'] = $sku['price'] * $fields['total'];
        $fields['member_id'] = $memberId;
        $fields['created_at'] = date('Y-m-d H:i:s');

        return self::insertGetId($fields);
    }

    public static function findProductsByIds($ids, $memberId)
    {
        return self::join('products', 'shop_cart.product_id', '=', 'products.id')
            ->join('product_sku', 'shop_cart.product_sku_id', '=', 'product_sku.id')
            ->select('shop_cart.*', 'products.thumbnail', 'products.name', 'products.product_category_id', 'product_sku.price as productPrice')
            ->where('shop_cart.member_id', $memberId)
            ->whereIn('shop_cart.id', $ids)
            ->get()->toArray();
    }

    public static function getByMemberId($memberId)
    {
        $cart = self::join('products', 'products.id', '=', 'shop_cart.product_id')
            ->join('product_sku', 'product_sku.id', '=', 'shop_cart.product_sku_id')
            ->where('member_id', $memberId)
            ->select('shop_cart.*', 'products.name', 'products.thumbnail', 'product_sku.price as product_price')
            ->get()->toArray();

        $ids = array_column($cart, 'id');
        self::where('member_id', $memberId)->whereNotIn('id', $ids)->delete();


        return $cart;
    }

    public function product()
    {
        return $this->belongsTo(Products::class);
    }

    public function product_sku()
    {
        return $this->belongsTo(ProductSku::class);
    }

    public function member()
    {
        return $this->belongsTo(Members::class);
    }
}
