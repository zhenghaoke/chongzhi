<?php

namespace App\Models\Article;

use App\Models\Model;

class Articles extends Model
{
    protected $table = 'articles';

    public static function get($categoryId)
    {
        return self::where('article_category_id', $categoryId)
            ->get(['id', 'title', 'desc', 'thumbnail', 'created_at'])->toArray();
    }

    public function addVisited($id)
    {
        $articleModel = self::where('id', $id);
        $article = $articleModel->get()->first();
        if (empty($article)) {
            throw new \Exception('找不到文章!');
        }

        return $articleModel->update($this->timestamp([
            'visited' => $article['visited'] + 1
        ], 1));
    }

    public function addShared($id)
    {
        $articleModel = self::where('id', $id);
        $article = $articleModel->get()->first();
        if (empty($article)) {
            throw new \Exception('找不到文章!');
        }

        return $articleModel->update($this->timestamp([
            'shared' => $article['shared'] + 1
        ], 1));
    }

    public static function getList($page, $limit = 10)
    {
        $articleModel = self::where('article_category_id', 2);
        $count = count($articleModel->get()->toArray());
        $articles = $articleModel->offset(($page - 1) * $limit)->limit($limit)->get()->toArray();

        return [$articles, $count];
    }
}
