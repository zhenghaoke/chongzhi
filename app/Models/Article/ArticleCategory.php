<?php

namespace App\Models\Article;

use App\Tool\ArrayTool;
use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    protected $table = 'article_category';

    public static function getOptions()
    {
        $options = self::orderBy('order', 'asc')->select(['id', 'title'])->get()->toArray();

        return ArrayTool::keyValuePair($options, 'id', 'title');
    }
}
