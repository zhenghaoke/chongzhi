<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners';

    public static function get()
    {
        $banners = self::orderBy('order', 'asc')->get()->toArray();

        return empty($banners) ? [] : array_column($banners, 'img_url');
    }
}
