<?php

namespace App\Models;

use App\Models\Product\Products;
use App\Models\Product\ProductSku;
use App\Models\Seckill\SeckillOrders;
use App\Models\Seckill\SeckillProducts;
use EasyWeChat\Factory;
use App\Models\Order\Orders;
use App\Models\Order\OrderItems;
use EasyWeChat\Kernel\Exceptions\Exception;
use Illuminate\Database\Eloquent\Model;

class Pay extends Model
{
    public function payByWeChat($orderSn, $memberId, $type)
    {
        if ($type == 'base') {
            $order = Orders::where('order_sn', $orderSn)->get()->first();
        }
        if ($type == 'seckill') {
            $order = SeckillOrders::where('order_sn', $orderSn)->get()->first();
        }
        $member = Members::where('id', $memberId)->get()->first();
        if (empty($order) || empty($member)) {
            throw new \Exception('找不到订单!');
        }

        $payment = Factory::payment(self::getConfig());
        $jssdk = $payment->jssdk;

        $result = $payment->order->unify($this->generatePayFields($order, $member['openid']));
        if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS') {
            $prepayId = $result['prepay_id'];
            return $jssdk->sdkConfig($prepayId);
//            return response($config);
        }

        if ($result['return_code'] == 'FAIL' && array_key_exists('return_msg', $result)) {
            throw new \Exception($result['return_msg']);
        }

        throw new \Exception($result['err_code_des']);
    }

    public static function notifyByWeChat()
    {
        $payment = Factory::payment(self::getConfig());
        $response = $payment->handlePaidNotify(function ($message, $fail) {
            // 根据返回的订单号查询订单数据
            $orderModel = Orders::where('order_sn', $message['out_trade_no']);
            $type = 'base';
            if (empty($orderModel->get()->first())) {
                $orderModel = SeckillOrders::where('order_sn', $message['out_trade_no']);
                $type = 'seckill';
            }
            $order = $orderModel->get()->first();
            if (empty($order)) {
                $fail('订单不存在!');
            }

            if ($order['status'] == '1') {
                return true;
            }

            // 支付成功后的业务逻辑
            if ($message['result_code'] === 'SUCCESS') {
                $order['order_sn'] = $message['out_trade_no']; // 回调订单号
                $order['pay_order_sn'] = $message['transaction_id']; // 回调流水号
                $order['pay_time'] = date('Y-m-d H:i:s');
                $order['status'] = 1;
                $order['updated_at'] = date('Y-m-d H:i:s');

                $orderModel->update($order);

                // 添加销量, 减少库存
                if ($type == 'base') {
                    Products::addSalesAndDecreaseStock($order['order_sn']);
                } else {
                    SeckillProducts::addSalesAndDecreaseStock($order['order_sn']);
                }
            }

            return true;
        });

        return $response;
    }

    private function generatePayFields($order, $openid)
    {
        return [
            'body' => env('APP_NAME'),         // 订单说明
            'out_trade_no' => $order['order_sn'],   // 平台内部订单号
            'total_fee' => $order['price'] * 100,   // 价格, 单位为分
//            'spbill_create_ip' => '123.12.12.123', // 可选，如不传该参数，SDK 将会自动获取相应 IP 地址
//            'notify_url' => 'https://pay.weixin.qq.com/wxpay/pay.action', // 支付结果通知网址，如果不设置则会使用配置里的默认地址
            'trade_type' => 'JSAPI', // 请对应换成你的支付方式对应的值类型 小程序为JSAPI
            'openid' => $openid,
        ];
    }

    public static function getConfig()
    {
        return [
            // 必要配置
            'app_id' => Configs::where('key', 'APP_ID')->value('value'),
            'mch_id' => Configs::where('key', 'MCH_ID')->value('value'),
            'key' => Configs::where('key', 'KEY')->value('value'),   // API 密钥

            // 如需使用敏感接口（如退款、发送红包等）需要配置 API 证书路径(登录商户平台下载 API 证书)
//            'cert_path' => 'path/to/your/cert.pem', // XXX: 绝对路径！！！！
//            'key_path' => 'path/to/your/key',      // XXX: 绝对路径！！！！

            'notify_url' => route('wechat.notify'),     // 你也可以在下单时单独设置来想覆盖它
//            'sandbox' => true, // 设置为 false 或注释则关闭沙箱模式
            'sandbox' => false, // 设置为 false 或注释则关闭沙箱模式
        ];
    }
}
