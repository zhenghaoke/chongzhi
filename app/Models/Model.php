<?php

namespace App\Models;

use App\Dbu\Dbu;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    private $dbu;

    public function __construct(array $attributes = [], Dbu $dbu = null)
    {
        $this->dbu = $dbu;
        parent::__construct($attributes);
    }

    protected function getDbu()
    {
        return $this->dbu;
    }

    protected function timestamp($fields, $type)
    {
        $time = date('Y-m-d H:i:s');
        if ($type) {
            $fields['updated_at'] = $time;
            return $fields;
        }
        $fields['created_at'] = $time;
        return $fields;
    }
}
