<?php

namespace App\Models;

use GuzzleHttp\Client;

class Sms extends Model
{
    public static function sendSms($url, $data = array())
    {
        $client = new Client();
        $response = $client->request('POST', $url, ['form_params' => $data]);
        $result = $response->getBody()->getContents();

        return self::getMessage()[$result];
    }

    public static function generate($mobile, $content, $suffix, $type=1)
    {
        $password = '9g28xz';//短信系统平台用户登录密码
        $key = '6d07a25df1d17cde8f6bd7bc21644ef0';//在接口触发页面可以获取

        $options['username'] = 'sms245427';//短信系统平台用户名即管理名称
        $options['time'] = time() - 8 * 3600;//当前格林尼治时间戳
        $options['content'] = urlencode($content . "【" . $suffix . "】");//需要中文输入法的左右括号，签名字数要在3-8个字
        $options['mobile'] = $mobile;//收信人手机号码
        $options['authkey'] = md5($options['username'] . $options['time'] . md5($password) . $key);
        $options['type'] = $type;

        return $options;
    }

    private static function getMessage()
    {
        return [
            '0.未知错误', '发送成功', '参数不正确', '验证失败', '用户名或密码错误',
            '数据库操作失败', '余额不足', '内容不符合格式', '频率超限',
            '接口超时', '后缀签名长度超过限制', '其他错误', '签名或内容格式不通过',
            '一个手机号码一天内超过5 次', '暴力破解被封一小时',
            '黑名单手机','16.未知错误', '短信格式匹配失败' ,'黑名单关键字',
            '提交ip没有在设置的白名单中', '手机号不在配的段中', '手机格式错误',
        ];
    }
}
