<?php

namespace App\Models;

use App\Tool\ArrayTool;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Address extends Model
{
    protected $table = 'address';

    public static function get($id, $memberId)
    {
        $address = self::where('id', $id)
            ->where('member_id', $memberId)
            ->get()->first();

        return empty($address) ? [] : $address->toArray();
    }

    public static function getDefault($memberId)
    {
        $address = self::where('default', 1)
            ->where('member_id', $memberId)
            ->get()->first();

        return empty($address) ? [] : $address->toArray();
    }

    public static function getOther($memberId)
    {
        $address = self::where('default', 0)
            ->where('member_id', $memberId)
            ->get()->toArray();

        return $address;
    }

    public function create($fields, $memberId)
    {
        $fields = ArrayTool::parts($fields, [
            'name', 'mobile',
            'region',
//            'province', 'city', 'area',
            'address', 'default'
        ]);
        $fields = $this->verificationFields($fields);
        DB::beginTransaction();
        try {
            $fields['member_id'] = $memberId;
            $fields['created_at'] = date('Y-m-d H:i:s');
            self::insertGetId($fields);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return true;
    }

    public function edit($fields, $memberId)
    {
        $fields = ArrayTool::parts($fields, [
            'id', 'name', 'mobile',
            'region',
//            'province', 'city', 'area',
            'address', 'default'
        ]);
        if (empty($fields['id']) || empty(self::where('id', $fields['id'])->where('member_id', $memberId)->get()->toArray())) {
            throw new \Exception('保存地址不存在!');
        }
        list($fields['province'], $fields['city'], $fields['area']) = $fields['region'];
        unset($fields['region']);
        DB::beginTransaction();
        try {
            $this->setDefault($fields, $memberId);
            $fields['updated_at'] = date('Y-m-d H:i:s');
            self::where('id', $fields['id'])->update($fields);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return true;
    }

    public function del($id, $memberId)
    {
        return self::where('id', $id)->where('member_id', $memberId)->delete();
    }

    private function verificationFields($fields)
    {
        if (empty($fields['name']) || empty($fields['mobile'])) {
            throw new \Exception('未填写联系人或联系方式!');
        }
        if (empty($fields['region']) || !is_array($fields['region']) || count($fields['region']) != 3) {
            throw new \Exception('未选择省市区!');
        }
        if (empty($fields['address'])) {
            throw new \Exception('未填写详细地址!');
        }
        list($fields['province'], $fields['city'], $fields['area']) = $fields['region'];
        unset($fields['region']);

        return $fields;
    }

    public function setDefault($fields, $memberId)
    {
        $address = self::where('member_id', $memberId)->where('id', $fields['id'])->get()->first();
        if (empty($address)) {
            throw new \Exception('找不到地址信息');
        }
        DB::beginTransaction();
        try {
            self::where('member_id', $memberId)->update([
                'default' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            $result = self::where('member_id', $memberId)
                ->where('id', $fields['id'])->update([
                    'default' => 1,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return true;
    }
}
