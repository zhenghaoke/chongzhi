<?php

namespace App\Models\Order;

use App\Models\Address;
use App\Models\MemberCoupon;
use App\Models\Member\Members;
use App\Models\Model;
use App\Models\OrderAddress;
use App\Models\Product\Products;
use App\Models\Product\ProductSku;
use App\Models\Seckill\SeckillOrderItems;
use App\Models\Seckill\SeckillOrders;
use App\Models\ShopCart;
use App\Tool\ArrayTool;
use App\Tool\Tool;
use Illuminate\Support\Facades\DB;

class Orders extends Model
{
    protected $table = 'orders';
    public static $types = ['等待充值', '充值中', '充值成功', '充值失败', '等待回调', -1 => '退款'];

    public function member()
    {
        return $this->belongsTo(Members::class);
    }

    public function product()
    {
        return $this->hasOne(Products::class, 'id', 'product_id');
    }

    public function address()
    {
        return $this->belongsTo(OrderAddress::class, 'order_address_id', 'id');
    }

    public static function orderDetail($orderSn, $id = null)
    {
        $order = self::join('members', 'orders.member_id', '=', 'members.id')
            ->select('orders.*', 'members.nickname');
        $product = self::join('order_items', 'order_items.order_id', '=', 'orders.id')
            ->join('members', 'orders.member_id', '=', 'members.id')
            ->join('product_sku', 'order_items.sku_id', '=', 'product_sku.id')
            ->join('products', 'product_sku.product_id', '=', 'products.id')
            ->select('products.*', 'product_sku.price as price', 'order_items.total', 'order_items.price as total_price');

        if ($orderSn) {
            $order = $order->where('orders.order_sn', $orderSn)->get()->first();
            $product = $product->where('orders.order_sn', $orderSn)->get()->toArray();
        } else {
            if (empty($id)) {
                throw new \Exception('orderSn or id not undefined!');
            }
            $order = $order->where('orders.id', $id)->get()->first();
            $product = $product->where('orders.id', $id)->get()->toArray();
        }

        if (empty($order)) {
            throw new \Exception('找不到该订单!');
        }

        $address = OrderAddress::where('id', $order['order_address_id'])->get()->first();
        if (!empty($order['coupon_id'])) {
            $coupon = MemberCoupon::join('coupon', 'member_coupon.coupon_id', 'coupon.id')
                ->select('coupon.*', 'member_coupon.id', 'member_coupon.expired', 'member_coupon.use')
                ->where('member_coupon.id', $order['coupon_id'])->get()->first();
        }

        return [
            'order' => $order->toArray(),
            'products' => $product,
            'address' => $address->toArray(),
            'coupon' => empty($order['coupon_id']) ? null : $coupon->toArray(),
            'status' => self::$types
        ];
    }

    public static function getBySn($orderSn)
    {
        return self::where('order_sn', $orderSn)->orWhere('pay_order_sn', $orderSn)->with('product')->get()->first();
    }

    public static function addTrackingSn($id, $trackingSn, $trackingName)
    {
        return self::where('id', $id)->update([
            'tracking_sn' => $trackingSn,
            'tracking_name' => $trackingName,
            'status' => 2,
            'remind' => 0,
            'ship_time' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }

    public function createOrder($id, $type, $mobile)
    {
        if (in_array($type, [1, 2])) {
            $product = ProductSku::join('products', 'products.id', 'product_sku.product_id')
                ->select('product_sku.*', 'products.id', 'products.type', 'products.product_type')
                ->where('product_sku.id', $id)
                ->where('product_type', $type)
                ->where('product_sku.is_enable', 1)
                ->where('products.is_enable', 1)
                ->get()->first();
        }
        if ($type == 3) {
            $product = Products::where('id', $id)
                ->where('product_type', $type)
                ->where('is_enable', 1)
                ->get()->first();
        }

        if (empty($product) || !Tool::is_mobile($mobile)) {
            throw new \Exception('找不到产品或无效手机号');
        }
        $mobileType = Tool::getAttribution($mobile);
        $title = empty($product['name']) ? $product['content'] : $product['name'];

        $name = Products::$type[$product['type']] . Products::$productType[$product['product_type']] . '充值' . $title;
        $orderSn = Tool::generateSn($product['type'] - 1);

        $order = [
            'order_sn' => $orderSn,
            'member_id' => \Auth::id() ?? 1,
            'product_id' => $product['id'],
            'phone' => $mobile,
            'attribution' => $mobileType['province'],
            'settlement_price' => $product['price'], // 订单价格
            'status' => 0, // 订单状态
            'created_at' => date('Y-m-d H:i:s'),
            'name' => $name,
        ];
        $id = self::insertGetId($order);

        return $orderSn;
    }

    public static function confirmReceipt($orderSn, $memberId)
    {
        return self::where('order_sn', $orderSn)
            ->where('member_id', $memberId)
            ->whereIn('status', [0, 1, 2])->update([
                'status' => 3,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public static function deleteOrder($orderSn, $memberId)
    {
        return self::where('order_sn', $orderSn)
            ->where('member_id', $memberId)->update([
                'trashed' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public static function remind($orderSn, $memberId)
    {
        return self::where('order_sn', $orderSn)
            ->where('member_id', $memberId)->update([
                'remind' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public static function afterSale($orderSn, $memberId)
    {
        return self::where('order_sn', $orderSn)
            ->where('member_id', $memberId)->update([
                'status' => 4,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public static function searchOrders($status, $memberId, $page, $limit = 10)
    {
        $fields = [
            'order_sn', 'pay_order_sn', 'pay_time', 'price', 'status', 'coupon_id',
            'created_at', 'order_address_id', 'tracking_sn', 'tracking_name', 'ship_time', 'remind'
        ];
        $orders = self::select(array_merge($fields, ['tongchuang']))
            ->where('trashed', 0)
            ->where('member_id', $memberId);
        $seckillOrders = SeckillOrders::select(array_merge($fields, [DB::raw('0 as tongchuang')]))
            ->where('trashed', 0)
            ->where('member_id', $memberId);

        if ($status != 'all') {
            $orders = $orders->where('status', (int)$status);
            $seckillOrders = $seckillOrders->where('status', (int)$status);
        }

        $orders = $seckillOrders->unionAll($orders);
        $count = count($orders->get()->toArray());
        $orders = $orders->offset(($page - 1) * $limit)->limit($limit)->get()->toArray();

        // 订单按照创建时间排序
        $createds = array_column($orders, 'created_at');
        array_multisort($createds, SORT_DESC, $orders);

        // 根据订单号获取订单项
        $orderSns = array_column($orders, 'order_sn');
        $items = OrderItems::getByOrderSns($orderSns);
        $seckillItems = SeckillOrderItems::getByOrderSns($orderSns);

        foreach ($orders as &$order) {
            $order['type'] = substr($order['order_sn'], 0, 2) == 'MX';
            $order['items'] = $order['type'] ? $seckillItems[$order['order_sn']] : $items[$order['order_sn']];

            if (!empty($items[$order['order_sn']])) {
                $order['old_price'] = array_sum(array_column($items[$order['order_sn']], 'price'));
            }
        }

        return ['orders' => $orders, 'count' => $count, 'status' => self::$types];
    }

    private function checkFields($fields, $memberId, $type)
    {
        $fields = ArrayTool::parts($fields, ['cart_ids', 'address_id', 'coupon_id', 'tongchuang', 'juntai']);
        if (empty($fields['cart_ids'])) {
            throw new \Exception('cart_ids不存在!');
        }
        $fields['cart_ids'] = is_array($fields['cart_ids']) ? $fields['cart_ids'] : explode(',', $fields['cart_ids']);
        $fields['cart_ids'] = $type ? $fields['cart_ids'] : json_encode($fields['cart_ids']);
        if (empty($fields['cart_ids'])) {
            throw new \Exception('cart_ids不存在!');
        }
        $address = Address::where('member_id', $memberId)->where('id', $fields['address_id'])->get()->first();
        if (empty($fields['address_id']) || empty($address)) {
            throw new \Exception('地址不存在!');
        }

        $coupon = MemberCoupon::join('coupon', 'member_coupon.coupon_id', 'coupon.id')
            ->select('coupon.*', 'member_coupon.id', 'member_coupon.expired', 'member_coupon.use')
            ->where('member_id', $memberId)->where('member_coupon.id', $fields['coupon_id'])->get()->first();
        if (!empty($fields['coupon_id']) && empty($coupon)) {
            throw new \Exception('优惠券不存在!');
        }
        $fields['coupon_id'] = empty($fields['coupon_id']) ? 0 : $fields['coupon_id'];

        $coin = Members::where('id', $memberId)->get('tongchuang')->first();
        if (!empty($fields['tongchuang']) && $fields['tongchuang'] > $coin['tongchuang']) {
            throw new \Exception('您的同创币不足!');
        }
        $fields['tongchuang'] = empty($fields['tongchuang']) ? 0 : $fields['tongchuang'];

        return [$fields, $address->toArray(), $coupon, $coin['tongchuang']];
    }
}
