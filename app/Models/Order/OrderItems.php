<?php

namespace App\Models\Order;

use App\Tool\ArrayTool;
use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    protected $table = 'order_items';

    public static function getByOrderSns(array $orderSns)
    {
        $items = self::join('product_sku', 'order_items.sku_id', 'product_sku.id')
            ->join('orders', 'orders.id', 'order_items.order_id')
            ->join('products', 'products.id', 'product_sku.product_id')
            ->select('order_items.*', 'name', 'products.thumbnail', 'content', 'order_sn')
            ->whereIn('order_sn', $orderSns)->get()->toArray();

        return ArrayTool::group($items, 'order_sn');
    }
}
