<?php

namespace App\Models;

use App\Models\Product\Products;
use App\Tool\ArrayTool;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class MemberCoupon extends Model
{
    protected $table = 'member_coupon';

    public static function get($memberId)
    {
        $coupon = self::join('coupon', 'coupon.id', '=', 'member_coupon.coupon_id')
            ->select('coupon.*', 'member_coupon.id', 'member_coupon.expired', 'member_coupon.use')
            ->where('member_id', $memberId)
            ->where('is_enable', 1)
            ->where('start', '<=', date('Y-m-d'))
            ->where('end', '>=', date('Y-m-d'))
            ->get()->toArray();

        return $coupon;
    }

    public static function getReceive($memberId)
    {
        $coupon = self::join('coupon', 'member_coupon.coupon_id', 'coupon.id')
            ->where('member_id', $memberId)
            ->get()->toArray();

        return empty($coupon) ? [] : ArrayTool::index($coupon, 'coupon_id');
    }

    public static function receive($memberId, $couponId)
    {
        $time = date('Y-m-d H:i:s');
        $couponModel = Coupon::where('id', $couponId)
            ->where('is_enable', 1)
            ->where('start', '<=', $time)
            ->where('end', '>=', date('Y-m-d'));
        $coupon = $couponModel->first();
        if (empty($coupon)) {
            throw new \Exception('找不到优惠券!');
        }

        $memberCoupon = self::where('member_id', $memberId)
            ->where('coupon_id', $couponId)->get()->toArray();
        if (!empty($memberCoupon)) {
            throw new \Exception('已领取过该优惠券!');
        }

        $fields = ['coupon_id' => $couponId, 'member_id' => $memberId, 'created_at' => $time];
        DB::beginTransaction();
        try {
            $couponModel->update(['total' => $coupon->total - 1, 'updated_at' => $time]);
            self::insertGetId($fields);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return true;
    }

    public static function isHas($productId)
    {
        $product = Products::where('id', $productId)->first();
        if (empty($product)) {
            throw new \Exception('产品不存在!');
        }
        $time = date('Y-m-d H:i:s');
        $coupon = Coupon::where('is_enable', 1)
            ->where('start', '<=', $time)
            ->where('end', '>=', date('Y-m-d'))
            ->where(function (Builder $query) use ($product) {
                $query->where('product_category_ids', "like", "%\"{$product['product_category_id']}\"%");
                $query->orWhere('product_ids', "like", "%\"{$product['id']}\"%");
            })
            ->get()->toArray();

        return empty($coupon) ? false : $coupon;
    }

    public static function getAvailableCoupon($productIds, $productCateIds, $memberId, $totalPrice = 0)
    {
        $productCoupon = self::join('coupon', 'coupon.id', '=', 'member_coupon.coupon_id')
            ->select('coupon.*', 'member_coupon.id', 'member_coupon.expired', 'member_coupon.use')
            ->where('member_id', $memberId)
            ->where('is_enable', 1)
            ->where('expired', 0)->where('use', 0)
            ->where('applicable_type', 0);
        $cateCoupon = self::join('coupon', 'coupon.id', '=', 'member_coupon.coupon_id')
            ->select('coupon.*', 'member_coupon.id', 'member_coupon.expired', 'member_coupon.use')
            ->where('member_id', $memberId)
            ->where('is_enable', 1)
            ->where('expired', 0)->where('use', 0)
            ->where('applicable_type', 1);

        if (!empty($totalPrice)) {
            $productCoupon = $productCoupon->where('full', '<', $totalPrice);
            $cateCoupon = $cateCoupon->where('full', '<', $totalPrice);
        }
        $cateCoupon = $cateCoupon->get()->toArray();
        $productCoupon = $productCoupon->get()->toArray();

        $availableCoupon = [];
        foreach ($productCoupon as $coupon) {
            $coupon['product_ids'] = json_decode($coupon['product_ids']);
            $ids = array_intersect($coupon['product_ids'], $productIds);
            if (!empty($ids)) {
                $availableCoupon[] = $coupon;
            }
        }
        foreach ($cateCoupon as $coupon) {
            $coupon['product_category_ids'] = json_decode($coupon['product_category_ids']);
            $ids = array_intersect($coupon['product_category_ids'], $productCateIds);
            if (!empty($ids)) {
                $availableCoupon[] = $coupon;
            }
        }

        return $availableCoupon;
    }
}
