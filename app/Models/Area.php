<?php

namespace App\Models;

use App\Tool\ArrayTool;

class Area extends Model
{
    protected $table = 'area';

    public static function getNameByIds($ids)
    {
        $name = self::whereIn('id', $ids)->get(['id', 'areaName'])->toArray();
        return empty($name) ? [] : array_column($name, 'areaName');
    }

    public static function getOptionsToCity()
    {
        $options = self::where('level', 2)->get(['id', 'areaName'])->toArray();

        return ArrayTool::keyValuePair($options, 'id', 'areaName');
    }

    public static function getProvinces()
    {
        $area = self::where('level', 1)->get(['id', 'areaName'])->toArray();

        return ArrayTool::keyValuePair($area, 'id', 'areaName');
    }

    public static function getAreaByParentId($id)
    {
        $parentId = self::where('id', $id)->value('parentId');
        $area = self::where('parentId', $parentId)->get(['id', 'areaName as text'])->toArray();

        return ArrayTool::keyValuePair($area, 'id', 'text');
    }

}
