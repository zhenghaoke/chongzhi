<?php

namespace App\Models;

use App\Tool\Tool;

class MobileValidationCode extends Model
{
    protected $table = 'mobile_validation_code';

    public static function createValidationCode($mobile, $suffix, $url)
    {
        if (empty($mobile)) {
            new \Exception('手机号不存在!');
        }
        $code = Tool::validationCode();
        self::insert([
            'mobile' => $mobile,
            'code' => $code,
            'time' => time(),
        ]);
        $content = "您此次的验证码为：{{$code}}，十分钟内有效！";
        $options = Sms::generate($mobile, $content, $suffix);

        return Sms::sendSms($url, $options);
    }
}
