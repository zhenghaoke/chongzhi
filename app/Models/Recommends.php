<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recommends extends Model
{
    protected $table = 'recommends';

    public static function get()
    {
        return self::where('is_enable', 1)->orderBy('order', 'asc')->get(['id', 'content'])->toArray();
    }
}
