<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupon';

    public static function get($page, $limit = 10)
    {
        $coupon = self::where('is_enable', 1)->where('total', '>', 0);
        $count = count($coupon->get()->toArray());
        $coupon = $coupon->offset(($page - 1) * $limit)->limit($limit)->get()->toArray();

        return [
            'count' => $count,
            'coupon' => $coupon,
        ];
    }

    public function getProductCategoryIdsAttribute($value)
    {
        if (!empty($value) && !is_array($value)) {
            $value = json_decode($value);
        }
        return $value;
    }

    public function setProductCategoryIdsAttribute($value)
    {
        $this->attributes['product_category_ids'] = json_encode($value);
    }

    public function getProductIdsAttribute($value)
    {
        if (!empty($value) && !is_array($value)) {
            $value = json_decode($value);
        }
        return $value;
    }

    public function setProductIdsAttribute($value)
    {
        $this->attributes['product_ids'] = json_encode($value);
    }
}
