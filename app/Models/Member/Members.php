<?php

namespace App\Models\Member;

use App\Models\CoinLogs;
use App\Models\MemberCoupon;
use App\Models\MobileValidationCode;
use App\Models\Model;
use App\Models\Order\Orders;
use App\Models\SignIn;
use App\Tool\ArrayTool;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Support\Facades\DB;

class Members extends Model implements Authenticatable
{
    use AuthenticableTrait;

    protected $table = 'members';

    protected $fillable = [
        'nickname', 'avatar', 'mobile', 'status', 'expire_time', 'openid',
    ];

    public function vip()
    {
        return $this->hasOne(MemberVip::class, 'order', 'vip_level');
    }

    public static function getMember()
    {
        return self::where('id', \Auth::id() ?? 1)->with('vip')->get()->first();
    }

    public function createMember($mobile, $code)
    {
        $result = MobileValidationCode::where('mobile', $mobile)
            ->where('code', $code)->orderBy('id', 'desc')->get()->first();
        if (empty($result) || $result['time'] + 600 < time()) {
            throw new \Exception('验证码错误!');
        }
        $prefix = substr($mobile, 0, 3);
        $suffix = substr($mobile, -4, 4);

        $user = Members::UpdateOrCreate(['mobile' => $mobile], [
            'mobile' => $mobile,
            'avatar' => '/default_avatar.jpg',
            'nickname' => $prefix . 'xxxx' . $suffix,
            'openid' => '',
        ]);

        return $user;
    }

    public function updateMobile($memberId, $mobile, $code)
    {
        $result = MobileValidationCode::where('mobile', $mobile)
            ->where('code', $code)->orderBy('id', 'desc')->get()->first();
        if (empty($result) || $result['time'] + 10 * 60 < time()) {
            throw new \Exception('验证码错误!');
        }
        $memberModel = self::where('mobile', $mobile);
        if (!empty($memberModel->get()->toArray())) {
            throw new \Exception('该手机已绑定!');
        }
        $memberModel = self::where('id', $memberId);
        if (empty($memberModel->get()->toArray())) {
            throw new \Exception('找不到用户!');
        }

        $result = $memberModel->update($this->timestamp(['mobile' => $mobile], 1));

        return empty($result) ? false : $memberModel->get()->first();
    }

    public function updateCoinByOrder($id, $type = '付款')
    {
        $order = Orders::where('id', $id)->get()->first();
        if (empty($order)) {
            return false;
        }
        $memberModel = self::where('id', $order['member_id']);
        $member = $memberModel->get()->first();
        if (empty($member)) {
            return false;
        }
        $log = ['member_id' => $order['member_id'], 'type' => 0];

        DB::beginTransaction();
        try {
            if ($order['juntai']) {
                $log['content'] = "订单{$order['order_sn']} {$type} 获得君泰币{$order['juntai']}";
                $log['num'] = -$order['juntai'];
                CoinLogs::insertGetId($this->timestamp($log, 0));
            }
            if ($order['tongchuang']) {
                $log['content'] = "订单{$order['order_sn']} {$type} 抵扣同创币{$order['tongchuang']}";
                $log['num'] = $order['tongchuang'];
                CoinLogs::insertGetId($this->timestamp($log, 0));
            }

            $memberModel->update($this->timestamp([
                'juntai' => $member['juntai'] + (int)$order['juntai'],
                'tongchuang' => $member['tongchuang'] - $order['tongchuang'],
            ], 1));

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }

        return true;
    }

    public function updateCoin($id, $fields)
    {
        $fields = ArrayTool::parts($fields, ['type', 'num', 'method']);
        $num = empty($fields['num']) ? 0 : $fields['num'];

        return empty($fields['method']) ? $this->addCoin($id, 2, $num) : $this->delCoin($id, 2, $num);
    }

    public function addCoin($memberId, $type, $num)
    {
        $memberModel = self::where('id', $memberId);
        $member = $memberModel->get()->first();
        if (empty($member) || !in_array($type, [0, 1, 2])) {
            return false;
        }
        $log = ['member_id' => $memberId, 'type' => $type, 'num' => $num];

        DB::beginTransaction();
        try {
            $log['content'] = "后台添加余额{$num}";
            CoinLogs::insertGetId($this->timestamp($log, 0));
            $memberModel->update($this->timestamp(['balance' => $member['balance'] + $num], 1));

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
            return false;
        }

        return true;
    }

    public function delCoin($memberId, $type, $num)
    {
        $memberModel = self::where('id', $memberId);
        $member = $memberModel->get()->first();
        if (empty($member) || !in_array($type, [0, 1, 2])) {
            return false;
        }

        $log = ['member_id' => $memberId, 'type' => $type, 'num' => -$num];

        DB::beginTransaction();
        try {
            if ($member['balance']  >=  $num ) {
                $log['content'] = "后台减少余额{$num}";
                CoinLogs::insertGetId($this->timestamp($log, 0));
                $memberModel->update($this->timestamp(['balance' => $member['balance'] - $num], 1));
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }

        return true;
    }
}
