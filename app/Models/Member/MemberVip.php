<?php

namespace App\Models\Member;

use App\Tool\ArrayTool;
use Illuminate\Database\Eloquent\Model;

class MemberVip extends Model
{
    protected $table = 'member_vip';

    public static function getOptions()
    {
        $options = self::all(['order', 'title'])->toArray();

        return ArrayTool::keyValuePair($options, 'order', 'title');
    }
}
