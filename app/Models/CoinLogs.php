<?php

namespace App\Models;

use App\Models\Member\Members;
use Illuminate\Database\Eloquent\Model;

class CoinLogs extends Model
{
    protected $table = 'coin_log';

    public static $types = ['君泰币', '同创币', '余额'];

    public static function getSignInLogs($page, $memberId, $limit = 10)
    {
        $logs = self::where('content', "签到")
            ->where('type', 0)
            ->where('member_id', $memberId);
        $count = count($logs->get()->toArray());
        $logs = $logs->offset(($page - 1) * $limit)->limit($limit)->get()->toArray();

        return ['count' => $count, 'logs' => $logs];
    }

    public static function countJuntaiNum($memberId)
    {
        $logs = self::where('type', 0)
            ->where('member_id', $memberId)
            ->get(['num'])->toArray();

        return empty($logs) ? 0 : array_sum(array_column($logs, 'num'));
    }

    public static function countTongchuangNum($memberId)
    {
        $logs = self::where('type', 1)
            ->where('member_id', $memberId)
            ->get(['num'])->toArray();

        return empty($logs) ? 0 : array_sum(array_column($logs, 'num'));
    }

    public static function juntaiLogs($memberId)
    {
        return self::where('type', 0)
            ->where('member_id', $memberId)
            ->get()->toArray();
    }

    public static function tongchuangLogs($memberId)
    {
        return self::where('type', 1)
            ->where('member_id', $memberId)
            ->get()->toArray();
    }

    public static function balanceLogs($memberId)
    {
        return self::where('type', 2)
            ->where('member_id', $memberId)
            ->get()->toArray();
    }

    public static function getLogs($page, $memberId, $limit = 10)
    {
        $logs = self::where('member_id', $memberId);
        $count = count($logs->get()->toArray());
        $logs = $logs->offset(($page - 1) * $limit)->limit($limit)->get()->toArray();

        return ['count' => $count, 'logs' => $logs];
    }

    public function member()
    {
        return $this->belongsTo(Members::class, 'member_id', 'id');
    }
}
