<?php

namespace App\Models\Product;

use App\Models\Area;
use App\Models\Order\Orders;
use App\Tool\ArrayTool;
use App\Tool\Tool;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';

    public static $productType = [1 => '自动', 2 => '手动', 3 => '活动'];
    public static $operators = [1 => '电信', 2 => '移动', 3 => '联通'];
    public static $type = [1 => '话费', 2 => '流量'];

    public function product_sku()
    {
        return self::hasMany(ProductSku::class, 'product_id', 'id');
    }

    public static function getProducts($mobile, $page = null, $limit = 10, $type = 1)
    {
        if (empty($mobile)) {
            $products = self::where('is_enable', 1)
                ->with(['product_sku' => function ($query) {
                    $query->where('is_enable', 1);
                    $query->orderBy('order');
                }])
                ->where('is_enable', 1)
                ->where('type', $type)
                ->offset(0)->limit($limit)->get()->toArray();
            return ['count' => $limit, 'products' => $products];
        }
        if (!Tool::is_mobile($mobile)) {
            throw new \Exception('无效手机号');
        }
        $products = self::where('is_enable', 1)
            ->with(['product_sku' => function ($query) {
                $query->where('is_enable', 1);
                $query->orderBy('order');
            }])
            ->where('is_enable', 1)
            ->where('type', $type);

        $area = Tool::getAttribution($mobile);
        if (!empty($area['city'])) {
            $cityId = Area::where('areaName', 'like', "%{$area['city']}%")->value('id');
            $products = $products->where('prohibit_area_ids', 'not like', '%"'. $cityId . '"%');
//            $products = $products->where('allow_area_ids', 'like', '%"'. $cityId . '"%');
        }

        if (!empty($area['sp'])) {
            $id = array_flip(self::$operators)[$area['sp']];
            $products = $products->where('operator', 'like', '%"'. $id . '"%');
        }

        $count = count($products->get()->toArray());
        if (!empty($page)) {
            $products = $products->offset(($page - 1) * $limit)->limit($limit)->get()->toArray();
        } else {
            $products = $products->get()->toArray();
        }

        return ['count' => $count, 'products' => $products];
    }

    public static function search($keyword, $page, $limit = 10)
    {
        $products = self::where('name', 'like', "%{$keyword}%")
            ->where('is_enable', 1);

        $count = count($products->get()->toArray());
        $products = $products->offset(($page - 1) * $limit)->limit($limit)
            ->get(['id', 'name', 'thumbnail', 'price'])->toArray();

        $productIds = array_column($products, 'id');
        $prices = ProductSku::getMinPriceByProductIds($productIds);

        foreach ($products as &$product) {
            $product['min_price'] = empty($prices[$product['id']]) ? 0 : $prices[$product['id']];
        }

        return [
            'count' => $count,
            'products' => $products,
        ];
    }

    public static function findByCategoryId($id, $page, $limit = 10)
    {
        $cate = ProductCategory::where('id', $id)->orWhere('parent_id', $id)->get()->toArray();
        $ids = [$id];
        if (!empty($cate)) {
            $ids = array_column($cate, 'id');
        }
        $products = self::whereIn('product_category_id', $ids)->where('is_enable', 1);
        $count = count($products->get()->toArray());
        $products = $products->offset(($page - 1) * $limit)->limit($limit)
            ->get(['id', 'name', 'thumbnail', 'price'])->toArray();

        $productIds = array_column($products, 'id');
        $prices = ProductSku::getMinPriceByProductIds($productIds);

        foreach ($products as &$product) {
            $product['min_price'] = empty($prices[$product['id']]) ? 0 : $prices[$product['id']];
        }

        return [
            'count' => $count,
            'products' => $products,
        ];
    }

    public static function getByCategoryId($id, $page, $limit = 10)
    {
        $products = self::where('is_enable', 1);
        if (!empty($id)) {
            $products = $products->where('product_category_id', $id);
        }

        $count = count($products->get()->toArray());
        $products = $products->offset(($page - 1) * $limit)->limit($limit)
            ->get(['id', 'name', 'thumbnail', 'price'])->toArray();

        $productIds = array_column($products, 'id');
        $prices = ProductSku::getMinPriceByProductIds($productIds);

        foreach ($products as &$product) {
            $product['min_price'] = empty($prices[$product['id']]) ? 0 : $prices[$product['id']];
        }

        return [
            'count' => $count,
            'products' => $products,
        ];
    }

    public static function productDetail($id)
    {
        $product = self::where('id', $id)->get()->first();
        if (empty($product)) {
            throw new \Exception('找不到产品！');
        }
        $skuCateIds = array_column($product['product_sku'], 'sku_category_id');
        $skuIds = array_column($product['product_sku'], 'sku_ids');
        $skuIds = array_reduce($skuIds, 'array_merge', []);

        $sku = Sku::whereIn('id', $skuIds)->get()->toArray();
        $gsku = ArrayTool::group($sku, 'sku_category_id');
        $skuCate = SkuCategoty::getOptions($skuCateIds);
        $productSku = ProductSku::where('product_id', $id)->get()->toArray();
        $minPrice = ProductSku::getMinPriceByProductIds([$id]);

        return [
            'product' => $product,
            'skuCate' => $skuCate,
            'sku' => $gsku,
            'ssku' => ArrayTool::index($sku, 'id'),
            'productSku' => $productSku,
            'min_price' => reset($minPrice),
        ];
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id', 'id');
    }

    public function getAllowAreaIdsAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setAllowAreaIdsAttribute($value)
    {
        $this->attributes['allow_area_ids'] = json_encode(array_filter($value));
    }

    public function getSkuIdsAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setSkuIdsAttribute($value)
    {
        $this->attributes['sku_ids'] = json_encode(array_filter($value));
    }

    public function getProhibitAreaIdsAttribute($value)
    {
        return array_values(json_decode($value, true) ?: []);
    }

    public function setProhibitAreaIdsAttribute($value)
    {
        $this->attributes['prohibit_area_ids'] = json_encode(array_values($value));
    }

    public function getOperatorAttribute($value)
    {
        return array_values(json_decode($value, true) ?: []);
    }

    public function setOperatorAttribute($value)
    {
        $this->attributes['operator'] = json_encode(array_filter($value));
    }


    public static function addSalesAndDecreaseStock($orderSn)
    {
        $products = Orders::join('order_items', 'order_items.order_id', '=', 'orders.id')
            ->join('product_sku', 'order_items.sku_id', '=', 'product_sku.id')
            ->join('products', 'product_sku.product_id', '=', 'products.id')
            ->where('orders.status', '>', 0)
            ->where('orders.order_sn', $orderSn)
            ->get(['products.*', 'order_items.total', 'product_sku.stock'])->toArray();
        foreach ($products as $product) {
            self::where('id', $product['id'])->update(['sales' => $product['sales'] + $product['total'], 'updated_at' => date('Y-m-d H:i:s')]);
            ProductSku::where('id', $product['id'])->update(['stock' => $product['stock'] - $product['total'], 'updated_at' => date('Y-m-d H:i:s')]);
        }

        return true;
    }

    public static function getOptions()
    {
        $options = self::all(['id', 'name'])->toArray();

        return ArrayTool::keyValuePair($options, 'id', 'name');
    }
}
