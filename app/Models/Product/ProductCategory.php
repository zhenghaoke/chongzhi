<?php

namespace App\Models\Product;

use App\Tool\ArrayTool;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use ModelTree {
        ModelTree::boot as treeBoot;
    }

    protected $table = 'product_category';

    public static function getOptions()
    {
        $options = self::select(['id', 'title'])->get()->toArray();

        return ArrayTool::keyValuePair($options, 'id', 'title');
    }

    public static function get()
    {
        $cate = self::orderBy('order', 'asc')->get(['id', 'title', 'parent_id'])->toArray();
        $cate = ArrayTool::group($cate, 'parent_id');
        $first = array_merge([['id' => 0, 'title' => '全部分类', 'parent_id' => 0]], reset($cate));

        return ['first' => $first, 'second' => $cate];
    }
}
