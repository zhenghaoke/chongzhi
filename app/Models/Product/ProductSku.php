<?php

namespace App\Models\Product;

use App\Tool\ArrayTool;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductSku extends Model
{
    protected $table = 'product_sku';

    public static function getBySkuIds($ids, $total)
    {
        $product = self::join('products', 'products.id', 'product_sku.product_id')
            ->select('products.*', 'product_sku.*', 'product_sku.price as price')
            ->where('product_sku.sku_ids', json_encode($ids))->get()->toArray();
        foreach ($product as &$item) {
            $item['total'] = $total;
        }

        return $product;
    }

    public static function findBySkuIds($ids, $total)
    {
        $product = self::join('products', 'products.id', 'product_sku.product_id')
            ->where('product_sku.sku_ids', $ids)->get(['product_sku.*', 'product_sku.id as product_sku_id'])->toArray();
        foreach ($product as &$item) {
            $item['total'] = $total;
        }

        return $product;
    }

    public static function getMinPriceByProductIds($productIds)
    {
        $prices = ProductSku::whereIn('product_id', $productIds)
            ->where('stock', '>', 0)
            ->where('price', '>', 0)
            ->get(['id', 'product_id', 'price'])
            ->toArray();
        $prices = ArrayTool::group($prices, 'product_id');
        foreach ($prices as &$price) {
            $price = min(array_column($price, 'price'));
        }

        return $prices;
    }

    public function getSkuIdsAttribute($value)
    {
        if (!empty($value) && !is_array($value)) {
            $value = json_decode($value);
        }
        return $value;
    }

    public function setSkuIdsAttribute($value)
    {
        $this->attributes['sku_ids'] = json_encode($value);
    }

    public function product()
    {
        return $this->belongsTo(Products::class, 'product_id', 'id');
    }

    public static function addFieldsByOnce($skuIds, $productId)
    {
        $skuIds = array_filter($skuIds);
        $skuNames = Sku::whereIn('id', $skuIds)->get(['id', 'name'])->toArray();
        $skuNames = ArrayTool::index($skuNames, 'id');
        $exist = Sku::join('product_sku', 'product_sku.sku_id', 'sku.id')
            ->select('sku.id', 'sku.name')
            ->where('product_sku.product_id', $productId)
            ->get()->toArray();
        $exist = ArrayTool::index($exist, 'id');
        $intersection = array_intersect_key($skuNames, $exist);
        $deleted = array_diff_key($exist, $intersection);
        $deleted = empty($deleted) ? [] : array_column($deleted, 'id');
        $inserted = array_diff_key($skuNames, $intersection);

        $fields = [];
        foreach ($inserted as $name) {
            $fields[] = [
                'sku_id' => $name['id'],
                'content' => $name['name'],
                'product_id' => $productId,
                'created_at' => date('Y-m-d H:i:s'),
            ];
        }

        DB::beginTransaction();
        try {
            self::where('product_id', $productId)
                ->whereIn('sku_id', $deleted)->delete();
            self::insert($fields);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public static function addFields($productSkus, $productId)
    {
        $skus = empty($productSkus) ? [] : array_column($productSkus, 'sku_ids');
        if (empty($skus)) return false;
        $skuIds = array_filter(array_reduce($skus, 'array_merge', []));
        $skuNames = Sku::whereIn('id', $skuIds)->get(['id', 'name'])->toArray();
        $skuNames = ArrayTool::index($skuNames, 'id');
        $sids = [];
        $contents = count($skus) == 1 ? self::getSkuContentsByOne($skuNames, $sids) : self::getSkuContents($skuNames, $skus, $sids);

        $hasContent = self::where('product_id', $productId)->where('stock', '>', '0')->where('price', '>', '0')->get(['content'])->toArray();
        $hasContent = array_column($hasContent, 'content');

        $fields = [];
        foreach ($contents as $key => $content) {
            if (in_array($content, $hasContent)) continue;
            $fields[] = [
                'content' => $content,
                'sku_ids' => json_encode($sids[$key]),
                'product_id' => $productId,
                'created_at' => date('Y-m-d H:i:s'),
            ];
        }
        $insertContent = array_diff($contents, $hasContent);

        DB::beginTransaction();
        try {
            self::where('product_id', $productId)->whereIn('content', $insertContent)->delete();
            self::insert($fields);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    public static function getSkuContents($skuNames, &$skus, &$ids = [], &$reset = [])
    {
        $reset = empty($reset) ? array_splice($skus, 0, 1) : [$reset]; // 获取首组规格元素
        $reset2 = array_splice($skus, 0, 1);// 获取二组规格以及之后元素
        $c = [];
        $i = [];
        foreach (reset($reset) as $key => $k) { // 规格两两合并
            if (empty($k)) continue;
            foreach (reset($reset2) as $r) {
                if (empty($r)) continue;
                if (empty($skuNames[$k]['name'])) { // 非第一次进入
                    $i[] = array_merge($ids[$key], [$r]);
                } else {
                    $i[] = [$k, $r];
                }
                $c[] = (empty($skuNames[$k]['name']) ? $k : $skuNames[$k]['name']) . '|' . $skuNames[$r]['name'];
            }
        }
        $ids = $i;

        if (!empty($skus)) {
            $c = self::getSkuContents($skuNames, $skus, $ids, $c);
        }

        return $c;
    }

    public static function getSkuContentsByOne($skuNames, &$ids = [])
    {
        $contents = [];
        foreach ($skuNames as $id => $name) {
            $ids[$id] = [$id];
            $contents[$id] = $name['name'];
        }

        return $contents;
    }

}
