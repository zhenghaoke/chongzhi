<?php

namespace App\Models\Product;

use App\Tool\ArrayTool;
use Illuminate\Database\Eloquent\Model;

class SkuCategoty extends Model
{
    protected $table = 'sku_categoty';

    public static function getOptions($ids = [])
    {
        $columns = ['id', 'title'];
        $options = empty($ids) ? self::all($columns) : self::whereIn('id', $ids)->get($columns);

        return ArrayTool::keyValuePair($options->toArray(), 'id', 'title');
    }
}
