<?php

namespace App\Models\Product;

use App\Tool\ArrayTool;
use Illuminate\Database\Eloquent\Model;

class Sku extends Model
{
    protected $table = 'sku';

    public static function getOptions($ids = [])
    {
        $columns = ['id', 'name'];
        $options = empty($ids) ? self::all($columns) : self::whereIn('id', $ids)->get($columns);

        return ArrayTool::keyValuePair($options, 'id', 'name');
    }

}
