<?php

namespace App\Models\Seckill;

use App\Tool\ArrayTool;
use Illuminate\Database\Eloquent\Model;

class SeckillOrderItems extends Model
{
    protected $table = 'seckill_order_items';

    public static function getByOrderSns(array $orderSns)
    {
        $seckillItems = self::join('seckill_products', 'seckill_order_items.seckill_product_id', 'seckill_products.id')
            ->join('seckill_orders', 'seckill_orders.id', 'seckill_order_items.seckill_order_id')
            ->select('seckill_order_items.*', 'name', 'thumbnail', 'order_sn')
            ->whereIn('order_sn', $orderSns)->get()->toArray();

        return ArrayTool::group($seckillItems, 'order_sn');
    }
}
