<?php

namespace App\Models\Seckill;

use App\Models\Address;
use App\Models\Member\Members;
use App\Models\OrderAddress;
use App\Models\Product\ProductCategory;
use App\Tool\Tool;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SeckillOrders extends Model
{
    protected $table = 'seckill_orders';
    public static $types = ['等待充值', '充值中', '充值成功', '充值失败', '等待回调', -1 => '退款'];

    public function member()
    {
        return $this->belongsTo(Members::class);
    }

    public function address()
    {
        return $this->belongsTo(OrderAddress::class, 'order_address_id', 'id');
    }

    public function pay_member()
    {
        return $this->belongsTo(Members::class, 'pay_member_id', 'id');
    }

    public static function findByCategoryId($id, $page, $limit = 10)
    {
        $cate = ProductCategory::where('id', $id)->where('parent_id', $id)->get()->toArray();
        $ids = [$id];
        if (!empty($cate)) {
            $ids = array_column($cate, 'id');
        }
        $products = self::whereIn('product_category_id', $ids)->where('is_enable', 1);
        $count = count($products->get()->toArray());
        $products = $products->offset(($page - 1) * $limit)->limit($limit)
            ->get(['id', 'name', 'thumbnail', 'price', 'old_price'])->toArray();

        return [
            'count' => $count,
            'products' => $products,
        ];
    }

    public static function confirmOrder($memberId, $productId, $addressId, $total)
    {
        // 默认地址
        $default = empty($addressId) ? Address::getDefault($memberId) : Address::get($addressId, $memberId);

        // 产品
        $product = SeckillProducts::where('id', $productId)
            ->select('id', 'name', 'thumbnail', 'price')
            ->get()->first();
        if (empty($product)) {
            throw new \Exception('该商品不存在!');
        }
        $product['price'] = $total * $product['price'];
        $product['total'] = $total;

        return ['default' => $default, 'product' => $product];
    }

    public static function createOrder($productId, $total, $addressId, $memberId)
    {
        $product = SeckillProducts::where('id', $productId)
            ->where('stock', '>=', $total)
            ->get()->first();
        if (empty($product)) {
            throw new \Exception('库存不足!');
        }

        $price = $total * $product['price'];
        // 订单项数据
        $item = [
            'seckill_product_id' => $productId,
            'total' => $total,
            'price' => $total * $product['price'],
            'created_at' => date('Y-m-d H:i:s')
        ];

        $address = Address::where('member_id', $memberId)->where('id', $addressId)->get()->first();
        if (empty($address)) {
            throw new \Exception('地址不存在!');
        }
        // 订单地址数据
        $orderSn = Tool::generateSn(1);
        $address['created_at'] = date('Y-m-d H:i:s');
        $address['order_sn'] = $orderSn;
        $address['address_id'] = $address['id'];
        unset($address['updated_at']);
        unset($address['member_id']);
        unset($address['default']);
        unset($address['id']);

        DB::beginTransaction();
        try {
            $addressId = OrderAddress::insertGetId($address->toArray());
            // 订单数据
            $order = [
                'order_sn' => $orderSn,
                'price' => $price, // 订单价格
                'status' => 0, // 订单状态
                'member_id' => $memberId,
                'coupon_id' => 0, // 优惠券id
                'order_address_id' => $addressId, // 订单地址id
                'created_at' => date('Y-m-d H:i:s')
            ];
            $orderId = self::insertGetId($order);
            $item['seckill_order_id'] = $orderId;
            SeckillOrderItems::insert($item);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return $orderSn;
    }

    public static function confirmReceipt($orderSn, $memberId)
    {
        return self::where('order_sn', $orderSn)
            ->where('member_id', $memberId)
            ->whereIn('status', [0, 1, 2])->update([
                'status' => 3,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public static function deleteOrder($orderSn, $memberId)
    {
        return self::where('order_sn', $orderSn)
            ->where('member_id', $memberId)->update([
                'trashed' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public static function remind($orderSn, $memberId)
    {
        return self::where('order_sn', $orderSn)
            ->where('member_id', $memberId)->update([
                'remind' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public static function afterSale($orderSn, $memberId)
    {
        return self::where('order_sn', $orderSn)
            ->where('member_id', $memberId)->update([
                'status' => 4,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public static function orderDetail($orderSn, $id = null)
    {
        $order = self::join('members', 'seckill_orders.member_id', '=', 'members.id')
            ->select('seckill_orders.*', 'members.nickname');
        $product = self::join('seckill_order_items', 'seckill_order_items.seckill_order_id', '=', 'seckill_orders.id')
            ->join('members', 'seckill_orders.member_id', '=', 'members.id')
            ->join('seckill_products', 'seckill_order_items.seckill_product_id', '=', 'seckill_products.id')
            ->select('seckill_products.*', 'seckill_order_items.total', 'seckill_order_items.price as total_price');

        if ($orderSn) {
            $order = $order->where('seckill_orders.order_sn', $orderSn)->get()->first();
            $product = $product->where('seckill_orders.order_sn', $orderSn)->get()->toArray();
        } else {
            if (empty($id)) {
                throw new \Exception('orderSn or id not undefined!');
            }
            $order = $order->where('seckill_orders.id', $id)->get()->first();
            $product = $product->where('seckill_orders.id', $id)->get()->toArray();
        }

        if (empty($order)) {
            throw new \Exception('找不到该订单!');
        }
        $address = OrderAddress::where('id', $order['order_address_id'])->get()->first();

        return [
            'order' => $order->toArray(),
            'address' => empty($address) ? [] : $address->toArray(),
            'products' => $product
        ];
    }

    public static function addTrackingSn($id, $trackingSn, $trackingName)
    {
        if (empty($trackingSn) || empty($trackingName)) {
            throw new \Exception('快递单号不可为空!');
        }
        return self::where('id', $id)->update([
            'tracking_sn' => $trackingSn,
            'tracking_name' => $trackingName,
            'status' => 2,
            'remind' => 0,
            'ship_time' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

    }

}
