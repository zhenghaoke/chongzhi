<?php

namespace App\Models\Seckill;

use App\Models\Product\ProductCategory;
use Illuminate\Database\Eloquent\Model;

class SeckillProducts extends Model
{
    protected $table = 'seckill_products';

    public static $types = [1 => '话费', 2 => '流量'];
    public static $operators = ['电信', '移动', '联通'];

    public static function search()
    {
        $products = self::where('is_enable', 1)
            ->where('start', '<=', date('Y-m-d H:i:s'))
            ->where('end', '>=', date('Y-m-d H:i:s'))
            ->where('stock', '>', 0);

        $count = count($products->get()->toArray());
        $products = $products->get()->toArray();

        return [
            'count' => $count,
            'products' => $products,
        ];
    }

    public static function productDetail($id)
    {
        $product = self::where('id', $id)->get()->first();
        if (empty($product)) {
            throw new \Exception('找不到产品！');
        }

        return [
            'product' => $product,
        ];
    }

    public static function get($cateId)
    {
        $product = self::where('start', '<=', date('Y-m-d H:i:s'))
            ->where('end', '>', date('Y-m-d H:i:s'))
            ->where('product_category_id', $cateId)
            ->where('stock', '>', 0)
            ->where('is_enable', 1)
            ->orderBy('order', 'asc')
            ->get(['id', 'name', 'thumbnail', 'end', 'price', 'old_price'])->toArray();

        return $product;
    }

    public static function addSalesAndDecreaseStock($orderSn)
    {
        $products = SeckillOrders::join('seckill_order_items', 'seckill_order_items.seckill_order_id', '=', 'seckill_orders.id')
            ->join('seckill_products', 'seckill_order_items.seckill_product_id', '=', 'seckill_products.id')
            ->where('seckill_orders.status', '>', 0)
            ->where('seckill_orders.order_sn', $orderSn)
            ->get(['seckill_products.*', 'seckill_order_items.total'])->toArray();
        foreach ($products as $product) {
            self::where('id', $product['id'])->update([
                'sales' => $product['sales'] + $product['total'],
                'stock' => $product['stock'] - $product['total'],
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }

        return true;
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id', 'id');
    }

    public function getDetailAttribute($value)
    {
        if (!empty($value) && !is_array($value)) {
            $value = json_decode($value);
        }
        return $value;
    }

    public function setDetailAttribute($value)
    {
        $this->attributes['detail'] = json_encode($value);
    }

    public function getBannersAttribute($value)
    {
        if (!empty($value) && !is_array($value)) {
            $value = json_decode($value);
        }
        return $value;
    }

    public function setBannersAttribute($value)
    {
        $this->attributes['banners'] = json_encode($value);
    }
}
