<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SignIn extends Model
{
    protected $table = 'sign_in';

    public function signIn($memberId)
    {
        if (!$this->repeat($memberId)) {
            throw new \Exception('今日您已经签过到了');
        }
        $fields = $this->generateFields($memberId);

        $num = 5;
        $flag = $fields['day'] % 7;
        if (in_array($flag, [2, 3, 5, 6])) {
            $num = 10;
        }
        if ($flag == 4) {
            $num = 30;
        }
        if ($flag == 0) {
            $num = 60;
        }
        $member = Members::where('id', $memberId)->get()->first();
        DB::beginTransaction();
        try {
            self::insertGetId($fields);
            CoinLogs::insertGetId([
                'content' => "签到获得君泰币{$num}",
                'num' => $num,
                'member_id' => $memberId,
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            Members::where('id', $memberId)->update([
                'juntai' => $member['juntai'] + $num,
                'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $num;
    }

    private function generateFields($memberId)
    {
        $time = strtotime(date('Y-m-d'));
        $end = date('Y-m-d H:i:s', $time - 1);
        $start = date('Y-m-d H:i:s', $time - 86400);
        $sign = self::whereBetween('created_at', [$start, $end])
            ->where('member_id', $memberId)
            ->get()->first();
        $fields = [
            'member_id' => $memberId,
            'created_at' => date('Y-m-d H:i:s'),
        ];
        $fields['day'] = empty($sign) ? 1 : $sign->day + 1;

        return $fields;
    }

    private function repeat($memberId)
    {
        $time = strtotime(date('Y-m-d'));
        $end = date('Y-m-d H:i:s', $time + 86399);
        $sign = self::whereBetween('created_at', [date('Y-m-d'), $end])
            ->where('member_id', $memberId)
            ->get()->first();

        return empty($sign);
    }
}
