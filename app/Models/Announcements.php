<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Announcements extends Model
{
    protected $table = 'announcements';

    public static function get()
    {
        return self::where('is_enable', 1)->orderBy('order', 'asc')
            ->get(['id', 'title', 'content'])->toArray();
    }
}
