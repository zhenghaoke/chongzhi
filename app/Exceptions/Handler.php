<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * 未报告的异常类型列表.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * 认证异常时不被flashed的数据.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * 上报异常至错误driver，如日志文件(storage/logs/laravel.log)，第三方日志存储分析平台
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * 将异常信息响应给客户端.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        $message = $exception->getMessage();
        $code = $exception->getCode();
        if ($code == 404 && empty($message)) {
            $message = '找不到页面';
        }

        return redirect(route('error', ['message' => $message]));
        return parent::render($request, $exception);
    }
}
