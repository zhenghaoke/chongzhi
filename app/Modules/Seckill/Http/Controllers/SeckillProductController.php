<?php

namespace App\Modules\Seckill\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Seckill\SeckillProducts;
use Illuminate\Http\Request;

class SeckillProductController extends Controller
{
    // 秒杀产品详情
    public function seckillDetail(Request $request, $id)
    {
        try {
            $product = SeckillProducts::productDetail($id);
        } catch (\Exception $e) {
            return ['code' => 200, 'message' => $e->getMessage()];
        }
        return ['code' => 200, 'data' => $product];
    }

    // 获取秒杀产品
    public function seckillSearch(Request $request)
    {
        $product = SeckillProducts::search();
        return ['code' => 200, 'data' => $product];
    }
}
