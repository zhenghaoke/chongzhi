<?php

namespace App\Modules\Seckill\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Seckill\SeckillOrders;
use Illuminate\Http\Request;

class SeckillOrderController extends Controller
{
    // 确认秒杀订单页面
    public function confirmSeckillOrder(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $productId = $request->query->get('product_id');
        $total = $request->query->get('total', 1);
        $addressId = $request->query->get('address_id', 0);

        $data = SeckillOrders::confirmOrder($memberId, $productId, $addressId, $total);

        return ['code' => 200, 'data' => $data];
    }

    // 创建秒杀订单
    public function createSeckill(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $productId = $request->request->get('product_id');
        $addressId = $request->request->get('address_id');
        $total = $request->request->get('total', 1);

        try {
            $orderSn = SeckillOrders::createOrder($productId, $total, $addressId, $memberId);
        } catch (\Exception $e) {
            return ['code' => 404, 'massage' => $e->getMessage()];
        }

        return ['code' => 200, 'massage' => '创建成功', 'order_sn' => $orderSn];
    }

    // 秒杀订单确认收货
    public function seckillReceipt(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $orderSn = $request->request->get('order_sn');

        $result = SeckillOrders::confirmReceipt($orderSn, $memberId);
        if (empty($result)) {
            return ['code' => 404, 'massage' => '操作失败!'];
        }

        return ['code' => 200, 'massage' => '操作成功!'];
    }

    // 删除秒杀订单
    public function seckillDelete(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $orderSn = $request->request->get('order_sn');

        $result = SeckillOrders::deleteOrder($orderSn, $memberId);
        if (empty($result)) {
            return ['code' => 404, 'massage' => '操作失败!'];
        }

        return ['code' => 200, 'massage' => '操作成功!'];
    }

    // 提醒发货秒杀订单
    public function seckillRemind(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $orderSn = $request->request->get('order_sn');

        $result = SeckillOrders::remind($orderSn, $memberId);
        if (empty($result)) {
            return ['code' => 404, 'massage' => '操作失败!'];
        }

        return ['code' => 200, 'massage' => '操作成功!'];
    }

    // 秒杀订单售后
    public function seckillAfterSale(Request $request)
    {
        $memberId = $request->attributes->get('memberId');
        $orderSn = $request->request->get('order_sn');

        $result = SeckillOrders::afterSale($orderSn, $memberId);
        if (empty($result)) {
            return ['code' => 404, 'massage' => '操作失败!'];
        }

        return ['code' => 200, 'massage' => '操作成功!'];
    }

    // 秒杀订单详情
    public function seckillDetail(Request $request)
    {
        $orderSn = $request->query->get('order_sn');

        try {
            $data = SeckillOrders::orderDetail($orderSn);
        } catch (\Exception $e) {
            return ['code' => 404, 'message' => $e->getMessage()];
        }

        return ['code' => 200, 'data' => $data];
    }
}
