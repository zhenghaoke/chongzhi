<?php

namespace App\Modules\Seckill\Http\Controllers\Admin;

use App\Models\Seckill\SeckillOrderItems;
use App\Models\Seckill\SeckillProducts;
use App\Modules\Seckill\Action\Order\Payment;
use App\Modules\Seckill\Action\Order\Refund;
use App\Modules\Seckill\Action\Order\Remark;
use App\Modules\Seckill\Action\Order\Ship;
use Encore\Admin\Controllers\HasResourceActions;
use App\Models\Seckill\SeckillOrders;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use App\Tool\Tool;

class SeckillOrdersController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('秒杀订单管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @param Request $request
     * @return Content
     */
    public function show($id, Content $content, Request $request)
    {
        if ($request->method() == 'POST') {
            $trackingSn = $request->request->get('tracking_sn');
            $trackingName = $request->request->get('tracking_name');
            SeckillOrders::addTrackingSn($id, $trackingSn, $trackingName);
        }

        return $content
            ->header(trans('秒杀订单管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('秒杀订单管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('秒杀订单管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SeckillOrders);

        $grid->model()->orderBy('remind', 'desc')
            ->orderBy('status', 'asc')
            ->whereNotIn('status', [4])
            ->orderBy('created_at', 'desc');
        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function (Grid\Filter $filter) {
                $filter->like('order_sn', '订单号');
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->where(function ($query) {
                    $ids = SeckillProducts::where('name', 'like', "%{$this->input}%")->select('id')->get()->toArray();
                    $ids = empty($ids) ? [] : array_column($ids, 'id');
                    $ids = SeckillOrderItems::whereIn('seckill_product_id', $ids)->select('seckill_order_id')->get()->toArray();
                    $ids = empty($ids) ? [] : array_column($ids, 'seckill_order_id');

                    $query->whereIn('id', $ids);
                }, '产品名称');
            });
        });
        $grid->id('ID');
        $grid->order_sn('订单号/通道单号');
        $grid->pay_order_sn('第三方订单号');
        $grid->column('member.nickname', '用户昵称');
        $grid->column('product.type', '充值业务')->display(function ($type) {
            return SeckillProducts::$types[$type];
        });
        $grid->status('订单类型')->display(function ($status) {
            return SeckillOrders::$types[$status];
        });
        $grid->pay_time('支付时间');
        $grid->settlement_price('订单金额');
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));
        $grid->disableCreateButton();
        $grid->disableBatchActions();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->add(new Remark);
            $actions->disableView();
            $actions->disableDelete();
            $actions->disableEdit();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return mixed
     * @throws \Exception
     */
    protected function detail($id)
    {
        $data = SeckillOrders::orderDetail(null, $id);
        $data['status'] = SeckillOrders::$types;
        $data['ships'] = Tool::ships();

        return view('admin/order/order-detail', $data);
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SeckillOrders);

        $form->display('id', 'ID');
        $form->display('order_sn', '订单号');
        $form->display('pay_order_sn', '微信流水号');
        $form->display('member.nickname', '用户昵称');
        $form->display('pay_member.nickname', '代下单用户昵称');
        $form->display('pay_time', '支付时间')->disable();
        $form->display('price', '订单金额');
        $form->radio('status', '订单状态')->options(SeckillOrders::$types);
//        $form->text('pay_type', 'pay_type');
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));

        return $form;
    }
}
