<?php

namespace App\Modules\Seckill\Http\Controllers\Admin;

use App\Models\Area;
use App\Models\Product\ProductCategory;
use App\Models\Seckill\SeckillProducts;
use App\Http\Controllers\Controller;
use App\Modules\Seckill\Action\Product\Delete;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class SeckillProductsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('秒杀商品管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('秒杀商品管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('秒杀商品管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('秒杀商品管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SeckillProducts);

        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('name', '产品名称');
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->like('category.title', '商品分类');
            });
        });
        $grid->id('ID');
        $grid->name('活动产品名称');
        $grid->column('category.title', '商品分类')->default('暂无分类');
        $grid->price('价格');
        $grid->num('验证码使用次数');
        $grid->operator('运营商')->display(function ($type) {
            return SeckillProducts::$operators[$type];
        });
        $grid->is_enable('是否上架')->display(function ($type) {
            return $type ? '上架' : '下架';
        });

        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));
        $grid->disableBatchActions();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableDelete();
            $actions->add(new Delete());
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SeckillProducts::findOrFail($id));

        $show->id('ID');
        $show->name('商品名称');
        $show->field('category.title', '产品分类')->default('暂无分类');
        $show->price('价格');
        $show->num('验证码使用次数');
        $show->detail('介绍');
        $show->allow_area_ids('允许充值地区')->as(function ($ids) {
            return implode(', ', Area::getNameByIds($ids));
        });
        $show->prohibit_area_ids('禁止充值地区')->as(function ($ids) {
            return implode(', ', Area::getNameByIds($ids));
        });

        $show->is_enable('是否上架')->as(function ($type) {
            return $type ? '上架' : '下架';
        });
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));
        $show->panel()->tools(function (Show\Tools $tools) {
            $tools->disableDelete();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SeckillProducts);

        $form->display('id', 'ID');
        $form->text('name', '商品名称')->required();
        $form->radio('type', '业务类型')->options(SeckillProducts::$types)->required();
        $form->text('price', '价格')->required();
        $form->textarea('detail', '介绍')->default('');
        $form->listbox('allow_area_ids', '允许充值地区')->options(Area::getOptionsToCity());
        $form->listbox('prohibit_area_ids', '禁止充值地区')->options(Area::getOptionsToCity());
        $form->number('num', '验证码使用次数')->default(1);
        $form->switch('is_enable', '是否上架')->default(0);
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));

        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        return $form;
    }
}
