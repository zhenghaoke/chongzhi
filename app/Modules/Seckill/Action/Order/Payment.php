<?php

namespace App\Modules\Seckill\Action\Order;

use App\Models\Seckill\SeckillOrders;
use Encore\Admin\Actions\RowAction;
use App\Models\Member\Members;

class Payment extends RowAction
{
    public $name = '付款';

    public function dialog()
    {
        $order = SeckillOrders::where('id', $this->getKey())->get()->first();
        $member = Members::where('id', $order['member_id'])->get()->first();
        $this->confirm("确定用户{$member['nickname']}已经付款成功?");
    }

    public function handle()
    {
        $result = SeckillOrders::where('id', $this->getRow()->id)
            ->update(['status' => 1, 'updated_at' => date('Y-m-d H:i:s')]);
        if (!$result) {
            return $this->response()->error('付款失败!')->refresh();
        }

        return $this->response()->success('付款成功!')->refresh();
    }
}
