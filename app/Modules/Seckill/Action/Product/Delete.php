<?php

namespace App\Modules\Seckill\Action\Product;

use App\Models\Seckill\SeckillOrders;
use App\Models\Seckill\SeckillProducts;
use Encore\Admin\Actions\RowAction;
use Illuminate\Support\Facades\DB;

class Delete extends RowAction
{
    public $name = '删除';

    public function dialog()
    {
        $this->confirm('确定' . $this->name . '？');
    }

    public function handle()
    {
        $id = $this->getRow()->id;
        $result = SeckillOrders::join('seckill_order_items', 'seckill_order_items.seckill_order_id', 'seckill_orders.id')
            ->join('seckill_products', 'seckill_order_items.seckill_product_id', 'seckill_products.id')
            ->whereNotIn('status', [0, 3])
            ->where('seckill_products.id', $id)
            ->get(['seckill_orders.order_sn'])->toArray();

        if (!empty($result)) {
            return $this->response()->error('删除失败!该商品还有秒杀订单处于未完成状态!')->refresh();
        }

        DB::beginTransaction();
        try {
            SeckillProducts::where('id', $id)->delete();
            DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
            return $this->response()->success('删除失败!')->refresh();
        }

        return $this->response()->success('删除成功!')->refresh();
    }
}
