<?php

use Illuminate\Routing\Router;

Route::group([
    'prefix'        => config('admin.route.prefix'),
//    'namespace'     => 'Modules\\Seckill\\Http\\Admin',
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {
    $router->resource('seckill/products', 'Admin\SeckillProductsController');
    $router->post('seckill/orders/{id}', 'Admin\SeckillOrdersController@show')->name('seckill.orders');
    $router->resource('seckill/orders', 'Admin\SeckillOrdersController');
    $router->post('seckill/after_sale/{id}', 'Admin\SeckillAfterSaleController@show')->name('seckill.after_sale');
    $router->resource('seckill/after_sale', 'Admin\SeckillAfterSaleController');
});

// 订单
Route::group([
    'middleware' => ['jwt.user'],
    'prefix' => 'orders',
], function (Router $router) {
    // 确认秒杀订单页
    $router->get('confirm_seckill', 'SeckillOrderController@confirmSeckillOrder');
    // 创建秒杀订单
    $router->post('create_seckill', 'SeckillOrderController@createSeckill');
    // 秒杀订单确认收货
    $router->post('seckill_receipt', 'SeckillOrderController@seckillReceipt');
    // 删除秒杀订单
    $router->post('delete_seckill', 'SeckillOrderController@seckillDelete');
    // 提醒秒杀订单发货
    $router->post('remind_seckill', 'SeckillOrderController@seckillRemind');
    // 秒杀订单详情页
    $router->get('seckill/detail', 'SeckillOrderController@seckillDetail');
    // 秒杀订单售后
    $router->get('seckill/after_sale', 'SeckillOrderController@seckillAfterSale');
});

// 产品
Route::group([
    'prefix' => 'product',
], function (Router $router) {
    // 秒杀订单详情页
    $router->get('seckill_detail/{id}', 'SeckillProductController@seckillDetail');
    // 获取秒杀产品
    $router->get('seckill_search', 'SeckillProductController@seckillSearch');
});

