<?php

namespace App\Admin\Actions\Order;

use App\Models\Order\Orders;
use App\Tool\Tool;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Remark extends RowAction
{
    public $name = '备注';

    public function handle(Model $model, Request $request)
    {
        $remark = $request->request->get('remark', '');

        try {
            $result = Orders::where('id', $model->id)->update([
                'remark' => $remark
            ]);
        } catch (\Exception $e) {
            return $this->response()->error('操作失败!')->refresh();
        }
        if (!$result) {
            return $this->response()->error('操作失败!')->refresh();
        }

        return $this->response()->success('操作成功!')->refresh();
    }

    public function form(Model $model)
    {
        $order = Orders::where('id', $model->id)->get(['remark'])->first();
        if (!empty($order)) {
            $this->textarea('remark', '备注信息')->default($order['remark'])->required();

        }
    }

}
