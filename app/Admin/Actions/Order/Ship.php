<?php

namespace App\Admin\Actions\Order;

use App\Models\Order\Orders;
use App\Tool\Tool;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Ship extends RowAction
{
    public $name = '发货';

    public function handle(Model $model, Request $request)
    {
        $id = $model->id;
        $sn = $request->request->get('sn');
        $name = $request->request->get('name');

        try {
            $result = Orders::addTrackingSn($id, $sn, $name);
        }catch (\Exception $e) {
            return $this->response()->error('发货失败!')->refresh();
        }
        if (!$result) {
            return $this->response()->error('发货失败!')->refresh();
        }

        return $this->response()->success('发货成功!')->refresh();
    }

    public function form()
    {
        $this->select('name', '物流公司类型')->options(Tool::ships())->required();
        $this->text('sn', '物流订单')->required();
    }

}
