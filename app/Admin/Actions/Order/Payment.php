<?php

namespace App\Admin\Actions\Order;

use App\Models\Member\Members;
use App\Models\Order\Orders;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class Payment extends RowAction
{
    public $name = '付款';

    public function dialog()
    {
        $order = Orders::where('id', $this->getKey())->get()->first();
        $member = Members::where('id', $order['member_id'])->get()->first();
        $this->confirm("确定用户{$member['nickname']}已经付款成功?");
    }

    public function handle()
    {
        \DB::beginTransaction();
        try{
            (new Members())->updateCoinByOrder($this->getRow()->id);
            Orders::where('id', $this->getRow()->id)
                ->update(['status' => 1, 'updated_at' => date('Y-m-d H:i:s')]);
            \DB::commit();
        }catch (\Exception $e) {
            \DB::rollBack();
            return $this->response()->error('付款失败!')->refresh();
        }

        return $this->response()->success('付款成功!')->refresh();
    }
}
