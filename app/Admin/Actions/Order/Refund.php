<?php

namespace App\Admin\Actions\Order;

use App\Models\Order\Orders;
use Encore\Admin\Actions\RowAction;

class Refund extends RowAction
{
    public $name = '退款';

    public function dialog()
    {
        $this->confirm('确定' . $this->name . '？');
    }

    public function handle()
    {
        $result = Orders::where('id', $this->getRow()->id)
            ->update(['status' => 3, 'updated_at' => date('Y-m-d H:i:s')]);
        if (!$result) {
            return $this->response()->error('退款失败!')->refresh();
        }

        return $this->response()->success('退款成功!')->refresh();
    }
}
