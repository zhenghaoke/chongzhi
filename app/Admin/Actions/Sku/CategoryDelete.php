<?php

namespace App\Admin\Actions\Sku;

use App\Models\Product\Products;
use App\Models\Product\ProductSku;
use App\Models\Product\Sku;
use Encore\Admin\Actions\RowAction;
use Illuminate\Support\Facades\DB;

class CategoryDelete extends RowAction
{
    public $name = '删除';

    public function dialog()
    {
        $this->confirm('确定' . $this->name . '？');
    }

    public function handle()
    {
        $id = $this->getRow()->id;
        $result = Sku::where('sku_category_id', $id)->get()->toArray();
        if (!empty($result)) {
            return $this->response()->error('删除失败!还有规格在使用该类型!')->refresh();
        }

        DB::beginTransaction();
        try {
            Products::where('id', $id)->delete();
            DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
            return $this->response()->success('删除失败!')->refresh();
        }

        return $this->response()->success('删除成功!')->refresh();
    }
}
