<?php

namespace App\Admin\Actions\Sku;

use App\Models\Product\Products;
use App\Models\Product\ProductSku;
use App\Models\Product\Sku;
use Encore\Admin\Actions\RowAction;
use Illuminate\Support\Facades\DB;

class Delete extends RowAction
{
    public $name = '删除';

    public function dialog()
    {
        $this->confirm('确定' . $this->name . '？');
    }

    public function handle()
    {
        $id = $this->getRow()->id;

        $result = ProductSku::where('sku_ids', 'like', "%{$id}%")->get()->toArray();
        if (!empty($result)) {
            return $this->response()->error('删除失败!还有产品在使用该规格!')->refresh();
        }

        DB::beginTransaction();
        try {
            Sku::where('id', $id)->delete();
            DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
            return $this->response()->error('删除失败!')->refresh();
        }

        return $this->response()->success('删除成功!')->refresh();
    }
}
