<?php

namespace App\Admin\Actions\Member;

use App\Models\Member\Members;
use Encore\Admin\Actions\RowAction;

class Enable extends RowAction
{
    public $name = '解封用户';

    public function dialog()
    {
        $this->confirm('确定解封该用户？');
    }

    public function handle()
    {
        $result = Members::where('id', $this->getRow()->id)
            ->update(['disable' => 0, 'updated_at' => date('Y-m-d H:i:s')]);
        if (!$result) {
            return $this->response()->error('解封失败!')->refresh();
        }

        return $this->response()->success('解封成功!')->refresh();
    }
}
