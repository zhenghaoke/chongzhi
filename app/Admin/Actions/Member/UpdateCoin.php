<?php

namespace App\Admin\Actions\Member;

use App\Models\Member\Members;
use App\Models\Model;
use Encore\Admin\Actions\RowAction;
use Illuminate\Http\Request;

class UpdateCoin extends RowAction
{
    public $name = '修改虚拟币';

    public function handle(Model $model, Request $request)
    {
        $fields = $request->request->all();
        $result = (new Members)->updateCoin($model->id, $fields);
        if (!$result) {
            return $this->response()->error('修改失败!')->refresh();
        }

        return $this->response()->success('修改成功!')->refresh();
    }

    public function form()
    {
//        $this->radio('type', '类型')->options(['君泰币', '同创币', '余额'])->required();
        $this->radio('method', '方法')->options(['增加', '减少'])->required();
        $this->text('num', '数量')->rules('required|numeric|gt:0');
    }
}
