<?php

namespace App\Admin\Actions\Member;

use App\Models\Member\Members;
use Encore\Admin\Actions\RowAction;

class Display extends RowAction
{
    public $name = '封禁用户';

    public function dialog()
    {
        $this->confirm('确定封禁该用户？');
    }

    public function handle()
    {
        $result = Members::where('id', $this->getRow()->id)
            ->update(['disable' => 1, 'updated_at' => date('Y-m-d H:i:s')]);
        if (!$result) {
            return $this->response()->error('封禁失败!')->refresh();
        }

        return $this->response()->success('封禁成功!')->refresh();
    }
}
