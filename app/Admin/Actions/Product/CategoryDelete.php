<?php

namespace App\Admin\Actions\Product;

use App\Models\Order\Orders;
use App\Models\Product\ProductCategory;
use App\Models\Product\Products;
use App\Models\Product\Sku;
use App\Models\Seckill\SeckillOrders;
use Encore\Admin\Actions\RowAction;
use Illuminate\Support\Facades\DB;
use Schema;

class CategoryDelete extends RowAction
{
    public $name = '删除';

    public function dialog()
    {
        $this->confirm('删除该分类会使得使用该分类的商品下架, 是否确认删除？');
    }

    public function handle()
    {
        $id = $this->getRow()->id;

        $orders = Orders::join('order_items', 'order_items.order_id', 'orders.id')
            ->join('product_sku', 'order_items.sku_id', 'product_sku.id')
            ->join('products', 'product_sku.product_id', 'products.id')
            ->where('products.product_category_id', $id)
            ->whereNotIn('status', [0, 3])
            ->get(['orders.order_sn'])->toArray();
        if (!empty($orders)) {
            return $this->response()->error('删除失败!该商品分类还有订单处于未完成状态!')->refresh();
        }

        DB::beginTransaction();
        try {
            Products::where('product_category_id', $id)->update([
                'product_category_id' => 0,
                'is_enable' => 0,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            ProductCategory::where('id', $id)->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->response()->error('删除失败!')->refresh();
        }

        return $this->response()->success('删除成功!')->refresh();
    }
}
