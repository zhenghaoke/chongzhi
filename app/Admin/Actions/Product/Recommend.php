<?php

namespace App\Admin\Actions\Product;

use App\Models\Product\Products;
use Encore\Admin\Actions\RowAction;

class Recommend extends RowAction
{
    // 在页面点击这一列的图表之后，发送请求到后端的handle方法执行
    public function handle(Products $product)
    {
        // 切换`recommend`字段的值并保存
        $product->recommend = (int) !$product->recommend;
        $product->save();

        // 保存之后返回新的html到前端显示
        $html = $product->recommend ? "<i class='fa fa-star'</i>" : "<i class='fa fa-star-o'></i>";

        return $this->response()->html($html);
    }

    // 这个方法来根据`recommend`字段的值来在这一列显示不同的图标
    public function display($recommend)
    {
        return $recommend ? "<i class='fa fa-star'></i>" : "<i class='fa fa-star-o'></i>";
    }
}
