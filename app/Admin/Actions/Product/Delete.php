<?php


namespace App\Admin\Actions\Product;


use App\Models\Order\Orders;
use App\Models\Product\Products;
use App\Models\Product\ProductSku;
use Encore\Admin\Actions\RowAction;
use Illuminate\Support\Facades\DB;

class Delete extends RowAction
{
    public $name = '删除';

    public function dialog()
    {
        $this->confirm('确定' . $this->name . '？');
    }

    public function handle()
    {
        $id = $this->getRow()->id;
        $result = Orders::join('order_items', 'order_items.order_id', 'orders.id')
            ->join('product_sku', 'order_items.sku_id', 'product_sku.id')
            ->join('products', 'product_sku.product_id', 'products.id')
            ->where('products.id', $id)
            ->whereNotIn('status', [0, 3])
            ->get(['orders.order_sn'])->toArray();

        if (!empty($result)) {
            return $this->response()->error('删除失败!该商品还有订单处于未完成状态!')->refresh();
        }

        DB::beginTransaction();
        try {
            ProductSku::where('product_id', $id)->delete();
            Products::where('id', $id)->delete();
            DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
            return $this->response()->success('删除失败!')->refresh();
        }

        return $this->response()->success('删除成功!')->refresh();
    }
}
