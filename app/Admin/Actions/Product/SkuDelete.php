<?php


namespace App\Admin\Actions\Product;


use App\Models\Order\Orders;
use App\Models\Product\Products;
use App\Models\Product\ProductSku;
use Encore\Admin\Actions\RowAction;
use Illuminate\Support\Facades\DB;

class SkuDelete extends RowAction
{
    public $name = '删除';

    public function dialog()
    {
        $this->confirm('确定' . $this->name . '？');
    }

    public function handle()
    {
        $id = $this->getRow()->id;
        $result = Orders::join('order_items', 'order_items.order_id', 'orders.id')
            ->join('product_sku', 'order_items.sku_id', 'product_sku.id')
            ->where('order_items.sku_id', $id)
            ->whereNotIn('status', [0, 3])
            ->get(['orders.order_sn'])->toArray();

        if (!empty($result)) {
            return $this->response()->error('删除失败!该商品还有订单处于未完成状态!')->refresh();
        }

        $result = ProductSku::where('id', $id)->delete();
        if (empty($result)) {
            return $this->response()->error('删除失败!')->refresh();
        }

        return $this->response()->success('删除成功!')->refresh();
    }
}
