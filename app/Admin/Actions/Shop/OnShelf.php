<?php

namespace App\Admin\Actions\Shop;

use Encore\Admin\Actions\RowAction;
use Illuminate\Support\Facades\DB;

class OnShelf extends RowAction
{
    public $name = '上架';

    public function dialog()
    {
        $this->confirm('确定上架？');
    }

    public function handle()
    {
        $id = $this->getRow()->id;
        $result = DB::table('shops')->where('id', $id)->update(['disable' => 0]);

        if (!$result) {
            return $this->response()->error('上架失败!')->refresh();
        }

        return $this->response()->success('上架成功!')->refresh();
    }


}
