<?php

namespace App\Admin\Actions\Shop;

use Encore\Admin\Actions\RowAction;
use Illuminate\Support\Facades\DB;

class OffShelf extends RowAction
{
    public $name = '下架';

    public function dialog()
    {
        $this->confirm('确定' . $this->name . '？');
    }

    public function handle()
    {
        $id = $this->getRow()->id;
        $result = DB::table('shops')->where('id', $id)->update(['disable' => 1]);

        if (!$result) {
            return $this->response()->error('下架失败!')->refresh();
        }

        return $this->response()->success('下架成功!')->refresh();
    }


}
