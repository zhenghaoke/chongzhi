<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');

# 用户
    $router->resource('/members', 'Member\MembersController');
    $router->resource('/member_vip', 'Member\MemberVipController');
    // TODO 用户权限

# 商品管理
    $router->resource('/product/category', 'Product\ProductCategoryController');
    $router->resource('/products', 'Product\ProductsController');
    $router->resource('/sku', 'Product\SkuController');
    $router->resource('/productAndSku', 'Product\ProductSkuController');

# 订单管理
    $router->post('orders/{id}', 'OrdersController@show')->name('orders.post');
    $router->resource('/orders', 'OrdersController');
    $router->post('after_sale/{id}', 'AfterSaleController@show')->name('after_sale.post');
    $router->resource('/after_sale', 'AfterSaleController');

# 资金管理
    $router->resource('/coin_logs', 'CoinLogsController');

# 网站默认配置
    $router->resource('configs', 'ConfigsController');
});


