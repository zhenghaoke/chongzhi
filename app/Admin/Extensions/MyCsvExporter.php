<?php

namespace App\Admin\Extensions;
use Encore\Admin\Grid\Column;
use Encore\Admin\Grid\Exporters\CsvExporter;

class MyCsvExporter extends CsvExporter
{
    public function export()
    {
        if ($this->callback) {
            call_user_func($this->callback, $this);
        }

        $response = function () {

            $handle = fopen('php://output', 'w');
            $titles = [];

            fputs($handle, chr(0xEF).chr(0xBB).chr(0xBF), 3);
            $this->chunk(function ($collection) use ($handle, &$titles) {
                Column::setOriginalGridModels($collection);

                $original = $current = $collection->toArray();

                $this->grid->getColumns()->map(function (Column $column) use (&$current) {
                    $current = $column->fill($current);
                    $this->grid->columnNames[] = $column->getName();
                });

                // Write title
                if (empty($titles)) {
                    fputcsv($handle, $titles = $this->getVisiableTitles());
                }

                // Write rows
                foreach ($current as $index => $record) {
                    fputcsv($handle, $this->getVisiableFields($record, $original[$index]));
                }
            });
            fclose($handle);
        };

        response()->stream($response, 200, $this->getHeaders())->send();

        exit;
    }
}