<?php

namespace App\Admin\Controllers\Article;

use App\Models\Article\ArticleCategory;
use App\Models\Article\Articles;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ArticlesController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('文章管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('文章管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('文章管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('文章管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Articles);

        $grid->id('ID');
        $grid->title('文章标题');
        $grid->article_category_id('文章分类')->display(function ($id) {
            $cate = ArticleCategory::where('id', $id)->get()->toArray();
            return empty($cate) ? '' : reset($cate)['title'];
        });
        $grid->thumbnail('缩略图')->image('', 80, 80);
//        $grid->type('文章类型');
        $grid->visited('访问量');
        $grid->shared('分享量');
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));
        $grid->disableBatchActions();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            if (in_array($this->getKey(), [1, 2, 3, 4, 5, 6])) {
                $actions->disableDelete();
            }
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Articles::findOrFail($id));

        $show->id('ID');
        $show->title('文章标题');
        $show->content('文章正文')->unescape()->as(function ($content) {
            return $content;
        });
        $show->article_category_id('文章分类')->as(function ($id) {
            $cate = ArticleCategory::where('id', $id)->get()->toArray();
            return empty($cate) ? '' : reset($cate)['title'];
        });
        $show->thumbnail('缩略图')->image();
//        $show->type('文章类型');
        $show->visited('访问量');
        $show->shared('分享量');
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));
        $show->panel()->tools(function (Show\Tools $tools) {
            $tools->disableDelete();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Articles);

        $form->display('id', 'ID');
        $form->text('title', '文章标题');
        $form->kindeditor('content', '文章正文');
        $form->select('article_category_id', '文章分类')->options(ArticleCategory::getOptions());
        $form->image('thumbnail', '缩略图');
//        $form->text('type', '文章类型');
        $form->display('visited', '访问量');
        $form->display('shared', '分享量');
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        return $form;
    }
}
