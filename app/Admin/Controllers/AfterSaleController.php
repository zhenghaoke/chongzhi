<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Order\Payment;
use App\Admin\Actions\Order\Refund;
use App\Admin\Actions\Order\Ship;
use App\Models\Member\Members;
use App\Models\Order\OrderItems;
use App\Models\Order\Orders;
use App\Models\Product\Products;
use App\Models\Product\ProductSku;
use App\Http\Controllers\Controller;
use App\Tool\Tool;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;

class AfterSaleController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('售后管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @param Request $request
     * @return Content
     */
    public function show($id, Content $content, Request $request)
    {
        if ($request->method() == 'POST') {
            $trackingSn = $request->request->get('tracking_sn');
            $trackingName = $request->request->get('tracking_name');
            Orders::addTrackingSn($id, $trackingSn, $trackingName);
        }

        return $content
            ->header(trans('售后管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('售后管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('售后管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Orders);

        $grid->model()->where('status',4);
        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function (Grid\Filter $filter) {
                $filter->like('order_sn', '订单号');
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->where(function ($query) {
                    $ids = Products::where('name', 'like', "%{$this->input}%")->select('id')->get()->toArray();
                    $ids = empty($ids) ? [] : array_column($ids, 'id');
                    $ids = ProductSku::whereIn('product_id', $ids)->select('id')->get()->toArray();
                    $ids = empty($ids) ? [] : array_column($ids, 'id');
                    $ids = OrderItems::whereIn('sku_id', $ids)->select('order_id')->get()->toArray();
                    $ids = empty($ids) ? [] : array_column($ids, 'order_id');

                    $query->whereIn('id', $ids);
                }, '产品名称');
            });
        });

        $grid->id('ID');
        $grid->order_sn('订单号');
        $grid->pay_order_sn('微信流水号');
        $grid->column('member.mobile', '用户名');
        $grid->column('member.nickname', '用户昵称');
        $grid->column('address.name', '联系人');
        $grid->column('address.mobile', '联系电话');
        $grid->pay_time('支付时间');
        $grid->price('订单金额');
        $grid->remark('备注');
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));
        $grid->disableCreateButton();
        $grid->disableBatchActions();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableDelete();
            $actions->disableEdit();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return mixed
     * @throws \Exception
     */
    protected function detail($id)
    {
        $data = Orders::orderDetail(null, $id);
        $data['ships'] = Tool::ships();

        return view('admin/order/order-detail', $data);
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Orders);

        $form->display('id', 'ID');
        $form->display('order_sn', '订单号');
        $form->display('pay_order_sn', '微信流水号');
        $form->display('member.nickname', '用户昵称');
        $form->display('pay_time', '支付时间')->disable();
        $form->display('price', '订单金额');
        $form->radio('status', '订单状态')->options(Orders::$types);
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));
        $form->saving(function (Form $form) {
            if ($form->status == 1) {
                $form->pay_time = date('Y-m-d H:i:s');
                $this->getModel()->updateCoinByOrder($form->model()->id);
            }
        });

        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        return $form;
    }

    /**
     * @return Members
     */
    private function getModel()
    {
        return $this->getDbu()->model('Members');
    }
}
