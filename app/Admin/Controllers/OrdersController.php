<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Order\Payment;
use App\Admin\Actions\Order\Remark;
use App\Admin\Extensions\MyCsvExporter;
use App\Models\Member\Members;
use App\Models\Order\Orders;
use App\Models\Product\ProductCategory;
use App\Http\Controllers\Controller;
use App\Models\Product\Products;
use App\Tool\Tool;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('订单管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @param Request $request
     * @return Content
     */
    public function show($id, Content $content, Request $request)
    {
        if ($request->method() == 'POST') {
            $trackingSn = $request->request->get('tracking_sn');
            $trackingName = $request->request->get('tracking_name');
            Orders::addTrackingSn($id, $trackingSn, $trackingName);
        }

        return $content
            ->header(trans('订单管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('订单管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('秒杀订单管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Orders);

        $grid->exporter('export');
        $grid->model()->orderBy('status', 'asc')
            ->orderBy('created_at', 'desc')
            ->whereNotIn('status', [4]);
        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();
            $filter->like('order_sn', '订单号');
        });

        $grid->id('ID');
        $grid->order_sn('订单号/通道单号');
        $grid->pay_order_sn('第三方订单号');
        $grid->column('member.nickname', '用户昵称');
        $grid->column('product.type', '充值业务')->display(function ($type) {
            return Products::$type[$type];
        });
        $grid->status('订单类型')->display(function ($status) {
            return Orders::$types[$status];
        });
        $grid->pay_time('支付时间');
        $grid->settlement_price('订单金额');
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));
        $grid->disableCreateButton();
        $grid->disableBatchActions();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->add(new Remark);
            if ($this->row->product->type == 2) {
                $actions->add(new Payment());
            }
            $actions->disableView();
            $actions->disableDelete();
            $actions->disableEdit();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return mixed
     * @throws \Exception
     */
    protected function detail($id)
    {
        $data = Orders::orderDetail(null, $id);
        $data['ships'] = Tool::ships();

        return view('admin/order/order-detail', $data);
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Orders);

        $form->display('id', 'ID');
        $form->display('order_sn', '订单号');
        $form->display('pay_order_sn', '微信流水号');
        $form->display('member.nickname', '用户昵称');
        $form->display('pay_time', '支付时间')->disable();
        $form->display('price', '订单金额');
        $form->radio('status', '订单状态')->options(Orders::$types);
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));
        $form->saving(function (Form $form) {
            if ($form->status == 1) {
                $form->pay_time = date('Y-m-d H:i:s');
                $this->getModel()->updateCoinByOrder($form->model()->id);
            }
        });

        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        return $form;
    }

    /**
     * @return Members
     */
    private function getModel()
    {
        return $this->getDbu()->model('Members');
    }
}
