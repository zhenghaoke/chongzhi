<?php

namespace App\Admin\Controllers\Product;

use App\Admin\Actions\Product\SkuDelete;
use App\Models\Product\Products;
use App\Models\Product\ProductSku;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ProductSkuController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('商品SKU管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('商品SKU管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('商品SKU管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('商品SKU管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ProductSku);

        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();
            $filter->where(function ($query) {
                $ids = Products::where('name', 'like', "%{$this->input}%")->select('id')->get()->toArray();
                $ids = empty($ids) ? [] : array_column($ids, 'id');

                $query->whereIn('product_id', $ids);
            }, '产品名称');
        });
        $grid->id('ID');
        $grid->column('product.name', '商品名称')->display(function ($name) {
            return empty($name) ? '该商品已被删除' : $name;
        });
        $grid->content('SKU名称');
        $grid->tag('标签');
        $grid->denomination('充值面值');
        $grid->cost('成本');
        $grid->price('售价');
        $grid->is_join('是否参与折扣')->display(function ($type) {
            return $type ? '参与' : '不参与';
        });
        $grid->is_enable('是否启用')->display(function ($type) {
            return $type ? '启用' : '禁用';
        });
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));
        $grid->disableBatchActions();
        $grid->disableCreateButton();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableDelete();
            $actions->add(new SkuDelete());
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ProductSku::findOrFail($id));

        $show->id('ID');
        $show->field('product.name', '商品名称')->as(function ($name) {
            return empty($name) ? '该商品已被删除' : $name;
        });
        $show->content('SKU名称');
        $show->tag('标签');
        $show->denomination('充值面值');
        $show->cost('成本');
        $show->price('售价');
        $show->is_join('是否参与折扣')->as(function ($type) {
            return $type ? '参与' : '不参与';
        });
        $show->is_enable('是否启用')->as(function ($type) {
            return $type ? '启用' : '禁用';
        });
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));
        $show->panel()->tools(function (Show\Tools $tools) {
            $tools->disableDelete();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ProductSku);

        $form->display('id', 'ID');
        $form->display('product.name', '商品名称')->default('该商品已被删除' );
        $form->display('content', 'SKU内容');
        $form->text('tag', '标签')->setLabelClass(['asterisk']);
        $form->text('denomination', '充值面值')->required();
        $form->text('cost', '成本')->required();
        $form->text('price', '售价')->required();
        $form->number('day', '限制时间(天)')->required();
        $form->number('num', '限制购买次数')->default(3);
        $form->text('order', '排序')->default(100);
        $form->switch('is_join', '是否参与折扣')->default(0);
        $form->switch('is_enable', '是否启用')->default(0);
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        $form->submitted(function (Form $form) {
            $data = request()->all();
            if (empty($data['tag'])) {
                return back()->withInput()->withErrors(['tag' => '标签不可为空!']);
            }

            return true;
        });

        return $form;
    }
}
