<?php

namespace App\Admin\Controllers\Product;

use App\Admin\Actions\Product\CategoryDelete;
use App\Models\Product\ProductCategory;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ProductCategoryController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('商品分类管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('商品分类管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('商品分类管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('商品分类管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ProductCategory);
        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();
            $filter->like('title', '分类名称');
        });
        $grid->model()->orderBy('order', 'asc');
        $grid->id('ID');
        $grid->title('分类名称');
        $grid->order('排序');
//        $grid->parent_id('上级分类')->display(function ($id) {
//            $category = ProductCategory::where('id', $id)->get()->toArray();
//            return empty($category) ? 'ROOT' : reset($category)['title'];
//        });
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));
        $grid->disableBatchActions();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
//            $actions->disableDelete();
        });
//        $grid->disableCreateButton();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ProductCategory::findOrFail($id));

        $show->id('ID');
        $show->title('分类名称');
        $show->order('排序');
//        $show->parent_id('上级分类')->as(function ($id) {
//            $category = ProductCategory::where('id', $id)->get()->toArray();
//            return empty($category) ? 'ROOT' : reset($category)['title'];
//        });
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));
        $show->panel()->tools(function (Show\Tools $tools) {
            $tools->disableDelete();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ProductCategory);

        $form->display('id', 'ID');
        $form->text('title', '分类名称');
//        $form->select('parent_id', '上级分类')->options(ProductCategory::selectOptions());
        $form->text('order', '排序')->default(100);
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));
        $form->hidden('parent_id')->default(0);
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        return $form;
    }
}
