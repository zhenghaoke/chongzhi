<?php

namespace App\Admin\Controllers\Product;

use App\Admin\Actions\Product\Delete;
use App\Admin\Actions\Product\Recommend;
use App\Models\Area;
use App\Models\Product\ProductCategory;
use App\Models\Product\Products;
use App\Http\Controllers\Controller;
use App\Models\Product\ProductSku;
use App\Models\Product\Sku;
use App\Models\Product\SkuCategoty;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    use HasResourceActions;

    private $flag = 0;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('商品管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('商品管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('商品管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('商品管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Products);

        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('name', '产品名称');
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->like('category.title', '商品分类');
            });
        });

        $grid->id('ID');
        $grid->name('商品名称');
        $grid->tag('标签');
        $grid->product_type('商品分类')->display(function ($type) {
            return Products::$productType[$type];
        });
//        $grid->operator('运营商')->display(function ($type) {
//            return Products::$operators[$type];
//        });

        $grid->price('价格');
        $grid->is_enable('是否上架')->display(function ($type) {
            return $type ? '上架' : '下架';
        });
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));
        $grid->disableBatchActions();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableDelete();
            $actions->add(new Delete());
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Products::findOrFail($id));

        $show->id('ID');
        $show->name('商品名称');
        $show->tag('标签');
        $show->product_type('商品分类')->as(function ($type) {
            return Products::$productType[$type];
        });
        $show->operator('运营商')->as(function ($ids) {
            $out = [];
            foreach ($ids as $id) {
                $out[] = Products::$operators[$id];
            }
            return implode(', ', $out);
        });
        $show->price('价格');
        $show->detail('介绍');
        $show->prohibit_area_ids('禁止充值地区')->as(function ($ids) {
            return implode(', ', Area::getNameByIds($ids));
        });

        $show->is_enable('是否上架')->as(function ($type) {
            return $type ? '上架' : '下架';
        });
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));
        $show->panel()->tools(function (Show\Tools $tools) {
            $tools->disableDelete();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Products);

        $form->display('id', 'ID');
        $form->text('name', '商品名称')->required();
        $form->text('tag', '标签')->required();
        $form->radio('product_type', '商品分类')->options(Products::$productType)
            ->when(3, function (Form $form) {
                $form->number('num', '验证码使用次数')->setLabelClass(['asterisk']);
            })
            ->when('in', [1, 2], function (Form $form) {
                $form->multipleSelect('sku_ids', '子产品')->options(Sku::getOptions())->setLabelClass(['asterisk']);
            })
            ->required();
        $form->multipleSelect('operator', '运营商')->options(Products::$operators)->required();
        $form->radio('type', '业务类型')->options(Products::$type)->required();
        $form->text('price', '价格')->required();

        $form->kindeditor('detail', '介绍')->required();
        $form->listbox('prohibit_area_ids', '禁止充值地区')->options(Area::getOptionsToCity());
        $form->switch('is_enable', '是否上架')->default(0);
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));

        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        $form->submitted(function (Form $form) {
            $data = request()->all();
            if (request()->route()->getActionMethod() == 'store') {
                if ($data['product_type'] == 3 && empty($data['num'])) {
                    $rules = ['num' => 'required'];
                }
                if (in_array($data['product_type'], [1, 2]) && empty($data['sku_ids'])) {
                    $rules = ['sku_ids' => 'required'];
                }

                $validator = \Validator::make($data, $rules ?? []);
                if (!$validator->passes()) {
                    // 返回错误信息给页面
                    return back()->withInput()->withErrors($validator);
                }
            } else {
                // 判断数据库中是否有数据，并判断是否有新图片上传
                if ($data['product_type'] == 3 && empty($data['num'])) {
                    $error = ['num' => '验证码使用次数不可为空!'];
                }
                if (in_array($data['product_type'], [1, 2]) && empty($data['sku_ids'])) {
                    $error = ['sku_ids' => '子产品不可为空!'];
                }
                if (!empty($error)) {
                    return back()->withInput()->withErrors($error);
                }
            }

            return true;
        });

        $form->saving(function (Form $form) {
            $id = $form->model()->id;
            $type = in_array($form->product_type, [1, 2]);
            if (!empty($id) && $type) {
                $skuIds = Products::where('id', $id)->value('sku_ids');
                $input = $form->sku_ids;
                $input = array_filter($input);
                if ($skuIds != $input) {
                    $this->flag = 1;
                }
            }
            if (empty($id) && $type) {
                $this->flag = 1;
            }
        });

        $form->saved(function (Form $form) {
            if ($this->flag) {
                ProductSku::addFieldsByOnce($form->sku_ids, $form->model()->id);
            }
        });

        return $form;
    }

    protected function skuList(Request $request)
    {
        return Sku::where('sku_category_id', $request->get('q'))->get(['id', 'name as text'])->toArray();
    }
}
