<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\CoinLogs;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CoinLogsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('客户资金明细'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('客户资金明细'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('客户资金明细'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('客户资金明细'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CoinLogs);

        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();
            $filter->column(0.5, function ($filter) {
                $filter->like('member.nickname', '用户昵称');
            });
            $filter->column(0.5, function ($filter) {
                $filter->between('created_at', '时间')->datetime();
            });

        });
        $grid->exporter('export');
        $grid->id('ID');
        $grid->column('member.nickname', '用户昵称');
        $grid->type('虚拟币类型')->display(function ($type) {
            return CoinLogs::$types[$type];
        });
        $grid->content('日志内容');
        $grid->num('虚拟币数量');
        $grid->created_at('记录时间')->sortable();
        $grid->disableCreateButton();
        $grid->disableBatchActions();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableEdit();
            $actions->disableDelete();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CoinLogs::findOrFail($id));

        $show->id('ID');
        $show->field('member.nickname', '用户昵称');
        $show->type('虚拟币类型')->as(function ($type) {
            return CoinLogs::$types[$type];
        });
        $show->content('日志内容');
        $show->num('虚拟币数量');
        $show->created_at('记录时间');
        $show->panel()->tools(function (Show\Tools $tools) {
            $tools->disableDelete();
            $tools->disableEdit();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        admin_toastr('Access error!', 'error');
        return redirect('/admin/configs');
        $form = new Form(new CoinLogs);

        $form->display('id', 'ID');
        $form->text('title', '公告标题');
        $form->textarea('content', '公告内容');
        $form->switch('is_enable', '是否启用');
        $form->text('order', '排序')->default(100);
//        $form->date('start', '开始日期');
//        $form->date('end', '结束日期');
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));

        return $form;
    }
}
