<?php

namespace App\Admin\Controllers;

use App\Models\ShopCart;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ShopCartController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('购物车管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('购物车管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('购物车管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('购物车管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ShopCart);

        $grid->id('ID');
        $grid->column('member.nickname', '用户昵称');
        $grid->column('product.name', '商品名称');
        $grid->column('product.thumbnail', '商品缩略图')->image('', 80,80);
        $grid->column('product_sku.content', '规格名称');
        $grid->column('product_sku.price', '单价');
        $grid->total('数量');
        $grid->price('总价');
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ShopCart::findOrFail($id));

        $show->id('ID');
        $show->field('member.nickname', '用户昵称');
        $show->field('product.name', '商品名称');
        $show->field('product.thumbnail', '商品缩略图')->image();
        $show->field('product_sku.content', '规格名称');
        $show->field('product_sku.price', '单价');
        $show->total('数量');
        $show->price('总价');
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ShopCart);

        $form->display('id', 'ID');
        $form->display('member.nickname', '用户昵称');
        $form->display('product.name', '商品名称');
        $form->image('product.thumbnail', '商品缩略图')->disable();
        $form->display('product_sku.content', '规格名称');
        $form->display('product_sku.price', '单价');
        $form->text('total', '数量');
        $form->text('price', '总价')->rules('numeric|min:0.01');
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));

        return $form;
    }
}
