<?php

namespace App\Admin\Controllers;

use App\Models\Coupon;
use App\Http\Controllers\Controller;
use App\Models\Product\ProductCategory;
use App\Models\Product\Products;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CouponController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('优惠券管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('优惠券管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('优惠券管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('优惠券管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Coupon);

        $grid->id('ID');
        $grid->title('优惠券名称');
//        $grid->type('优惠券类型')->display(function ($type) {
//            return $type ? '折扣' : '满减';
//        });
//        $grid->full('full');
//        $grid->price('price');
//        $grid->discount('discount');
        $grid->applicable_type('适用商品类型')->display(function ($type) {
            return $type ? '自选分类' : '自选商品';
        });
        $grid->start('起始日期');
        $grid->end('截止日期');
        $grid->total('发放数量');
        $grid->is_enable('状态')->display(function ($type) {
            return $type ? '启用' : '禁用';
        });
//        $grid->product_category_ids('product_category_ids');
//        $grid->product_ids('product_ids');
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Coupon::findOrFail($id));

        $show->id('ID');
        $show->title('优惠券名称');
        $show->type('优惠券类型')->as(function ($type) {
            return $type ? '折扣' : '满减';
        });
//        $grid->full('full');
//        $grid->price('price');
//        $grid->discount('discount');
        $show->applicable_type('适用商品类型')->as(function ($type) {
            return $type ? '自选分类' : '自选商品';
        });

        $show->start('起始日期');
        $show->end('截止日期');
        $show->total('发放数量');
        $show->is_enable('状态')->as(function ($type) {
            return $type ? '启用' : '禁用';
        });
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Coupon);

        $form->display('id', 'ID');
        $form->text('title', '优惠券名称');
//        $type = $form->radio('type', '优惠券类型')
//            ->options(['满减', '折扣'])
//            ->when(0, function (Form $form) {
//                $form->text('full', '起用价格')->rules('required|integer');
//                $form->text('price', '抵扣价格')->rules('required|integer');
//            })
//            ->when(1, function (Form $form) {
//                $form->rate('discount', '折扣')->rules('required|integer');
//            })->rules('required');

        $form->hidden('type');
        $form->text('full', '起用价格')->rules('required|integer')->rules('required');
        $form->text('price', '抵扣价格')->rules('required|integer')->rules('required');


//        if (!$form->isEditing()) {
//            $type->when('', function (Form $form) {
//                $form->text('full', '起用价格')->rules('required|integer');
//                $form->text('price', '抵扣价格')->rules('required|integer');
//            });
//        }
        $applicableType = $form->radio('applicable_type', '适用商品类型')
            ->options(['自选商品', '自选分类'])
            ->when(0, function (Form $form) {
                $form->listbox('product_ids', '自选商品')->options(Products::getOptions());
            })
            ->when(1, function (Form $form) {
                $form->multipleSelect('product_category_ids', '自选分类')->options(ProductCategory::getOptions());
            });
        if (!$form->isEditing()) {
            $applicableType->when(0, function (Form $form) {
                $form->listbox('product_ids', '自选商品')->options(Products::getOptions());
            });
        }

        $form->date('start', '起始日期');
        $form->date('end', '截至日期');
        $form->text('total', '发放数量')->default(0);
        $form->switch('is_enable', '状态')->default(0);
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));
        $form->saving(function ($form) {
            $form->type = 0;
        });

        return $form;
    }
}
