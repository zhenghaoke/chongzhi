<?php

namespace App\Admin\Controllers;

use App\Models\Announcements;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AnnouncementsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('公告管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('公告管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('公告管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('公告管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Announcements);

        $grid->model()->orderBy('order', 'asc');
        $grid->id('ID');
        $grid->title('公告标题');
//        $grid->content('');
        $grid->order('排序');
        $grid->is_enable('是否启用')->display(function ($type) {
            return $type ? '启用' : '禁用';
        });
//        $grid->start('start');
//        $grid->end('end');
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Announcements::findOrFail($id));

        $show->id('ID');
        $show->title('公告标题');
        $show->content('公告内容');
        $show->is_enable('是否启用')->as(function ($type) {
            return $type ? '启用' : '禁用';
        });
//        $show->start('start');
//        $show->end('end');
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Announcements);

        $form->display('id', 'ID');
        $form->text('title', '公告标题');
        $form->textarea('content', '公告内容');
        $form->switch('is_enable', '是否启用');
        $form->text('order', '排序')->default(100);
//        $form->date('start', '开始日期');
//        $form->date('end', '结束日期');
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));

        return $form;
    }
}
