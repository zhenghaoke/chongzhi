<?php

namespace App\Admin\Controllers\Member;

use App\Admin\Actions\Member\Display;
use App\Admin\Actions\Member\Enable;
use App\Admin\Actions\Member\UpdateCoin;
use App\Models\Member\Members;
use App\Http\Controllers\Controller;
use App\Models\Member\MemberVip;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class MembersController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('会员管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('会员管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('会员管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('会员管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Members);

        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();
            $filter->like('nickname', '昵称');
        });

        $grid->id('ID');
        $grid->nickname('昵称');
        $grid->avatar('头像')->image('', 50);
        $grid->mobile('手机');
//        $grid->openid('openid');
        $grid->balance('余额');
        $grid->column('vip.title', 'vip等级')->default('暂无等级');
        $grid->spread_level('推广等级');
        $grid->ip('上次登录ip');
        $grid->login_time('上次登录时间');
        $grid->disable('用户状态')->display(function ($type) {
            return $type ? '封禁' : '正常使用';
        });
        $grid->created_at(trans('admin.created_at'));
        $grid->actions(function ($actions) {
            $actions->add(new UpdateCoin());
            if ($this->row->disable) {
                $actions->add(new Enable());
            } else {
                $actions->add(new Display());
            }
            $actions->disableDelete();
        });
        $grid->disableCreateButton();
        $grid->disableBatchActions();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Members::findOrFail($id));

        $show->id('ID');
        $show->nickname('昵称');
        $show->avatar('头像')->image();
        $show->mobile('联系方式');
        $show->openid('微信openId');
        $show->balance('余额');
        $show->field('vip.title', 'vip等级')->as(function ($name) {
            return empty($name) ? '暂无等级' : $name;
        });
        $show->spread_level('推广等级');
        $show->ip('上次登录ip');
        $show->login_time('上次登录时间');
        $show->disable('用户状态')->display(function ($type) {
            return $type ? '封禁' : '正常使用';
        });
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        $show->panel()->tools(function (Show\Tools $tools) {
            $tools->disableDelete();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Members);

        if (!$form->isEditing()) {
            admin_toastr('Access error!', 'error');
            return redirect('/admin/members');
        }

        $form->display('id', 'ID');
        $form->text('nickname', '昵称')->required();
        $form->image('avatar', '头像')->required();
        $form->display('openid', '微信openid');
        $form->text('mobile', '手机')->rules('required|unique:members,mobile,{{id}}|numeric:size:11', ['required' => '此项不能为空','unique' => '手机号不能重复']);;
        $form->select('vip_level', 'vip等级')
            ->options(MemberVip::getOptions())->default(0);
        $form->text('spread_level', '推广等级')->default(0);
        $form->text('balance', '余额')->rules('numeric|min:0')->default(0);
        $form->display('ip', '上次登录ip');
        $form->display('login_time', '上次登录时间');
        $form->switch('disable', '是否封禁')->default(0);
        $form->display('updated_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));

        $form->footer(function ($footer) {
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });

        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        return $form;
    }
}
