<?php

namespace App\Admin\Controllers\Member;

use App\Models\Member\MemberVip;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class MemberVipController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('会员等级管理'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('会员等级管理'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('会员等级管理'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('会员等级管理'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new MemberVip);

        $grid->model()->orderBy('order');
        $grid->id('ID');
        $grid->title('会员名称');
        $grid->order('等级');
        $grid->price('价格阈值');
        $grid->discount('优惠(%)');
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(MemberVip::findOrFail($id));

        $show->id('ID');
        $show->title('会员名称');
        $show->order('等级');
        $show->price('价格阈值');
        $show->discount('优惠(%)');
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new MemberVip);

        $form->display('id', 'ID');
        $form->text('title', '会员名称')->required();
        $form->text('order', '等级')->default(0)
            ->rules('required|unique:member_vip,order,{{id}}', ['required' => '此项不能为空','unique' => '会员等级不能重复']);
        $form->text('price', '价格阈值')->default(500);
        $form->text('discount', '优惠(%)')
            ->rules('required|numeric|between:0,100')
            ->default(100);
        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));

        return $form;
    }
}
