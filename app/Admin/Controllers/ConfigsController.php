<?php

namespace App\Admin\Controllers;

use App\Models\Configs;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ConfigsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('网站默认配置'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('微信配置设置'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('微信配置设置'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('微信配置设置'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Configs);

        $grid->id('ID');
        $grid->name('配置项名称');
        $grid->key('配置项');
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->disableBatchActions();
        $grid->disableCreateButton();
        $grid->actions(function (Grid\Displayers\DropdownActions $actions) {
            $actions->disableDelete();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Configs::findOrFail($id));

        $show->id('ID');
        $show->name('配置项名称');
        $show->key('配置项');
        $show->value('配置项内容');

        $show->panel()->tools(function ($tools) {
            $tools->disableDelete();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Configs);
        if (!$form->isEditing()) {
            admin_toastr('Access error!', 'error');
            return redirect('/admin/configs');
        }

        $form->display('id', 'ID');
        $form->display('name', '配置项名称');
        $form->display('key', '配置项');
        if (request()->route()->parameters['config'] == 4) {
            $form->switch('value', '配置项内容')->setLabelClass(['asterisk'])->default(1);
        } else {
            $form->text('value', '配置项内容')->required();
        }

        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
        });

        $form->disableCreatingCheck();
        $form->disableEditingCheck();
        $form->disableViewCheck();

        return $form;
    }
}
